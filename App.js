/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react'
import { NavigationContainer } from '@react-navigation/native';
import LoggedIn from './Pages/LoggedIn';
import { useDispatch } from 'react-redux';
import { getAllRewards } from './Pages/redux/Actions/RewardsAction'
import { requestUserPermission, notificationListner } from './Pages/Notification/Notifications'
import AsyncStorageStatic from '@react-native-community/async-storage'
import { checkLogin, sharedTicketDetails } from './Pages/redux/Actions/userAction';
import ForegroundHandler from './Pages/Notification/ForegroundHandler';
import dynamicLinks from '@react-native-firebase/dynamic-links';
export default function App() {
  const dispatch = useDispatch()
  const fetchToken = async () => {
    const token = await AsyncStorageStatic.getItem('User');
    dispatch(checkLogin(token))
  }
  const getAppLaunchLink = async () => {
    try {
      const { url } = await dynamicLinks().getInitialLink();
      getQueryParams(url)
    } catch (error) {
      // console.log(error, 'errors')
    }
  };
  const handleDynamicLink = link => {
    getQueryParams(link.url)
  };
  function getQueryParams(url) {
    var qparams = {},
      parts = (url || '').split('?'),
      qparts, qpart,
      i = 0;
    if (parts.length <= 1) {
      return qparams;
    } else {
      qparts = parts[1].split('&');
      for (i in qparts) {
        qpart = qparts[i].split('=');
        qparams[decodeURIComponent(qpart[0])] =
          decodeURIComponent(qpart[1] || '');
      }
    }
    return dispatch(sharedTicketDetails(qparams));
  };
  useEffect(() => {
    const unsubscribe = dynamicLinks().onLink(handleDynamicLink);
    dispatch(getAllRewards())
    fetchToken()
    getAppLaunchLink()
    requestUserPermission()
    notificationListner()
    return () => unsubscribe()
  }, [])
  return (
    <NavigationContainer>
      <ForegroundHandler />
      <LoggedIn />
    </NavigationContainer>
  )
}

