import axios from "axios";
import AsyncStorageStatic from '@react-native-community/async-storage'
import { getAwardPointsbyuserId } from "./PointsAction";
export const GET_USER_BY_ID = 'GET_USER_BY_ID'
export const LOGIN = 'LOGIN'
export const CHECK_LOGIN = 'CHECK_LOGIN'
export const LOGOUT = 'LOGOUT'
export const SET_AWARDS = 'SET_AWARDS'
export const ERRORS = 'ERRORS'
export const REGISTER_ERRORS = 'REGISTER_ERRORS'

export const UserRegister = (userData, fn) => async dispatch => {
    let fcmToken = await AsyncStorageStatic.getItem('fcmToken')
    var config = {
        method: 'post',
        url: 'https://5c9twl6zfb.execute-api.ap-south-1.amazonaws.com/dev/api/v1/users/register',
    };
    try {
        const { data } = await axios.post(config.url, { ...userData, token: fcmToken })
        if (data) {
            if (fn) fn('Login')
        }
    } catch (error) {
        dispatch({ type: ERRORS, payload: error.response.data.error })
    }
}
export const UserLogin = (userdata) => async dispatch => {
        const fcmToken = await AsyncStorageStatic.getItem('fcmToken')
    var config = {
        method: 'post',
        url: 'https://5c9twl6zfb.execute-api.ap-south-1.amazonaws.com/dev/api/v1/users/login',
    };
    try {
        const { data } = await axios.post(config.url, { ...userdata, token: fcmToken })
        if (data) {
            dispatch({ type: LOGIN, payload: data })
            dispatch({ type: GET_USER_BY_ID, payload: data.data })
            await AsyncStorageStatic.setItem('User', JSON.stringify(data.token))
            await AsyncStorageStatic.setItem('UserId', JSON.stringify(data.data._id))
            dispatch(getAwardPointsbyuserId(data.data._id))
        }
    } catch (error) {
        console.log(error.response.data)
        dispatch({ type: ERRORS, payload: error.response.data.error })
    }
}
export const getUsersbyId = (userId) => async dispatch => {
    var config = {
        method: 'get',
        url: 'https://5c9twl6zfb.execute-api.ap-south-1.amazonaws.com/dev/api/v1/users',
           };
    try {
        const { data } = await axios.get(`${config.url}/${userId}`)
        dispatch({ type: GET_USER_BY_ID, payload: data })
    } catch (error) {
        console.log(error.response.data)
    }
}
export const UpdateUser = (alldata) => async dispatch => {
    if (alldata.value === 'gender') {
        var userData = {
            'gender': alldata.valueGender
        }
    } else if (alldata.value === 'address') {
        const { details, city, pincode, stateof, landmark } = alldata.fullAddress
        var userData = {
            'address': JSON.stringify({
                'details': details,
                "city": city,
                "pincode": pincode,
                'state': stateof,
                'landmark': landmark
            })
        }
    }
    else if (alldata.value === 'dateOfBirth') {
        var userData = {
            'dateOfBirth': `${alldata?.alldate?.year}-${alldata?.alldate?.month}-${alldata?.alldate?.date}`
        }
    }
    const userId = JSON.parse(await AsyncStorageStatic.getItem('UserId'))
    var config = {
        method: 'put',
        url: 'https://5c9twl6zfb.execute-api.ap-south-1.amazonaws.com/dev/api/v1/users',
        headers: {
            'Content-Type': 'application/json'
        },
    };
    try {
        const { data } = await axios.put(`${config.url}/${userId}`, userData)
        userId && dispatch(getUsersbyId(userId))
        // console.log(data)
    } catch (error) {
        console.log(error.response)
    }
}
export const checkLogin = (data) => async dispatch => {
    const userId = JSON.parse(await AsyncStorageStatic.getItem('UserId'))
    const total = JSON.parse(await AsyncStorageStatic.getItem('newPoints'))
    userId && dispatch(getUsersbyId(userId))
    userId && dispatch(getAwardPointsbyuserId(userId))
    dispatch({ type: 'NEW_POINTS', payload: total })
    dispatch({ type: CHECK_LOGIN, payload: data })
}
export const logout = () => async dispatch => {
    await AsyncStorageStatic.removeItem('User')
    await AsyncStorageStatic.removeItem('UserId')
    await AsyncStorageStatic.removeItem('awardPoints')
    await AsyncStorageStatic.removeItem('newPoints')
    await AsyncStorageStatic.removeItem('fcmToken')
    dispatch({ type: LOGOUT })
}
export const removeError = () => async dispatch => {
    dispatch({ type: 'REMOVE_ERROR' })
}
export const sharedTicketDetails = (data) => async dispatch => {
    dispatch({ type: 'SHARED_ID_SAVE', payload: data })
}
