import axios from "axios";
import { getAwardPointsbyuserId, UpdatePoints } from './PointsAction'
import moment from "moment";
import AsyncStorageStatic from '@react-native-community/async-storage'
import { FILE_TICKETS } from "./FileuploadAction";
import { hideLoader, showLoader } from "./loaderAction";
export const TICKET_DELETE = 'TICKET_DELETE'
export const GET_TICKETS_BY_ID = 'GET_TICKETS_BY_ID'


export const UploadScanSmsTickets = (data) => async dispatch => {
    dispatch(showLoader())
    var Ticketdata =
    {
        "Body": data.values.state,
        "From": data.phoneNo,
        "language": data.values.language
    }
    var config = {
        method: 'post',
        url: 'http://3.110.83.166:8080/parsesms',
    };
    try {
        const { data } = await axios.post(config.url, Ticketdata)
        if (data.error) {
            dispatch({ type: 'SCANNED_FAILED_SMS', payload: data.error })
        }
        if (data.movieName) {
            const results = await axios.get(`https://imdb-api.com/en/API/Search/k_8qg681nx/${data.movieName}`)
            if (results) {
                const resultImage = results.data.results[0].image
                const result = await axios.put(`https://5c9twl6zfb.execute-api.ap-south-1.amazonaws.com/dev/api/v1/tickets/${data._id}`, { ticketImage: resultImage })
                if (result.data) {
                    dispatch({ type: FILE_TICKETS, payload: result.data })
                    console.log(result.data, 'helo')
                } else if (data) {
                    dispatch({ type: FILE_TICKETS, payload: data })
                }
            }
        } else {
            dispatch({ type: FILE_TICKETS, payload: data })
            // dispatch({ type: 'SCANNED_FAILED_SMS', payload: data.error })
            // dispatch(hideLoader())

        }
        // dispatch({ type: FILE_TICKETS, payload: data })
        dispatch(hideLoader())
    } catch (error) {
        console.log(error, 'UploadScanSmsTickets')
        dispatch(hideLoader())
        dispatch({ type: 'SCANNED_FAILED_SMS', payload: error })
    }
}
export const ticketDelete = () => dispatch => {
    dispatch({ type: TICKET_DELETE })
}
export const getTicketStatusUnclaimed = (dataid) => async dispatch => {
    try {
        dispatch(UpdatePoints({ ...dataid, ticketId: dataid._id }))
    } catch (error) {
        console.log(error.response)
    }
}

export const getTicketStatus = (dataid) => async dispatch => {
    var config = {
        method: 'put',
        url: 'https://5c9twl6zfb.execute-api.ap-south-1.amazonaws.com/dev/api/v1/tickets/',
    };
    try {
        const { data } = await axios.put(`${config.url}${dataid._id}`, { ...dataid, status: "In-process" })
        dispatch(UpdatePoints({ ...dataid, ticketId: dataid._id }))
    } catch (error) {
        console.log(error.response)
    }
}
export const getTicketStatusResult = (ticketdata) => async dispatch => {
    const userId = await AsyncStorageStatic.getItem('UserId')
    const { movieName, city, theater, _id, screenType, bookingTime,
        locale, ticketNumbers, bookingDate } = ticketdata.ticketId
    if (ticketdata.ticketId.platform === 'bookmyshow') {
        var formdata = {
            "awardId": ticketdata._id,
            "location": city,
            "movie": movieName,
            "cinemaHall": theater,
            "MovieDate": moment(bookingDate).format('YYYYMMDD'),
            "seatNumber": ticketNumbers,
            "language": locale + '-' + screenType,
            "time_": bookingTime,
            "ticket_id": _id,
            "userId": userId.replace('"', '').replace('"', '')
        }
        var configbms = {
            method: 'post',
            url: 'http://3.110.83.166:8080/bmsparser',
        };
        try {
            const { data } = await axios.post(configbms.url, formdata)
            data && dispatch(getAwardPointsbyuserId(userId.replace('"', '').replace('"', '')))
            console.log('bms')
        } catch (error) {
            console.log('getTicketStatusResult', error)
        }
    } else {
        var formdata =
        {
            "awardId": ticketdata._id,
            "location": city,
            "movie": movieName,
            "cinemaHall": theater,
            "MovieDate": moment(bookingDate).format('YYYYMMDD'),
            "seatNumber": ticketNumbers,
            "language": locale,
            "time_": bookingTime,
            "ticket_id": _id,
            "userId": userId.replace('"', '').replace('"', '')
        }
        var config = {
            method: 'post',
            url: 'http://3.110.83.166:8080/paytmparser',
        };
        try {
            const { data } = await axios.post(config.url, formdata)
            data && dispatch(getAwardPointsbyuserId(userId.replace('"', '').replace('"', '')))
            console.log(data, 'paytm')
        } catch (error) {
            console.log('getTicketStatusResult', error)
        }
    }
}

export const unclaimedTicketClaim = (userTicket) => async dispatch => {
    const userId = JSON.parse(await AsyncStorageStatic.getItem('UserId'))
    // console.log({userTicket})

    var formdata1 = {
        "attributedFrom": userTicket.userId,
        "userId": `${userId}`,
        "eventName": "UNCLAIMED_TICKET_CLAIMED"
    }
    var config = {
        method: 'put',
        url1: `https://5c9twl6zfb.execute-api.ap-south-1.amazonaws.com/dev/api/v1/tickets/${userTicket.ticketId}`,
    };
    try {
        const { data } = await axios.put(config.url1, formdata1)
        // console.log(data, 'ticketShareOther')
    } catch (error) {
        console.log(error.response.data, 'ticketShareOther')
    }
}
export const getTicketbyId = (ticketId) => async dispatch => {
    var config = {
        method: 'get',
        url: `https://5c9twl6zfb.execute-api.ap-south-1.amazonaws.com/dev/api/v1/tickets/${ticketId}`,
    };
    try {
        const { data } = await axios.get(`${config.url}`)
        dispatch({ type: GET_TICKETS_BY_ID, payload: data })
    } catch (error) {
        console.log(error.response, 'error')
    }
}