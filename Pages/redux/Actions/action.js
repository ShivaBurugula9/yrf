/* eslint-disable prettier/prettier */
export const OPEN_DRAWER = 'OPEN_DRAWER';
export const CLOSE_DRAWER = 'CLOSE_DRAWER';
export const REFRESH = 'REFRESH';


export const openDrawer = () => dispatch => {
    dispatch({ type: OPEN_DRAWER })
}
export const closeDrawer = () => dispatch => {
    dispatch({ type: CLOSE_DRAWER })
}
export const changeRefresh = (data) => dispatch => {
    dispatch({ type: REFRESH, payload: data })
}