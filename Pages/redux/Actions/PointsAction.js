import axios from 'axios'
import AsyncStorageStatic from '@react-native-community/async-storage'
import { getTicketStatusResult } from './TicketsAction'
export const NEW_POINTS = 'NEW_POINTS'
export const HISTORY = 'HISTORY'


export const UpdatePoints = (allData) => async dispatch => {
    const userId = JSON.parse(await AsyncStorageStatic.getItem('UserId'))
    var pointsdata = {
        "ticketId": allData.ticketId || null,
        "userId": userId,
        "rewardId": allData.rewardId || null,
        "points": allData.points || null,
        "type": "forUser",
        "eventName": allData.eventName
    };
    var config = {
        method: 'post',
        url: 'https://5c9twl6zfb.execute-api.ap-south-1.amazonaws.com/dev/api/v1/awarededPoints',
    };
    try {
        const { data } = await axios.post(config.url, pointsdata)
        // console.log(data, 'UpdatePoints')
        data.ticketId && dispatch(getTicketStatusResult(data))
        data && dispatch(getAwardPointsbyuserId(userId))
    } catch (error) {
        console.log(error)
    }

}
export const getAwardPointsbyuserId = (userId) => async dispatch => {
    var config = {
        method: 'get',
        url: 'https://5c9twl6zfb.execute-api.ap-south-1.amazonaws.com/dev/api/v1/awarededPoints?userId=',
    };
    try {
        const { data } = await axios.get(`${config.url}${userId}`)
        const mapData = data.data.map(item => item.points)
        const reducer = (previousValue, currentValue) => previousValue + currentValue;
        const total = mapData.reduce(reducer)
        dispatch({ type: HISTORY, payload: data.data })
        dispatch({ type: 'NEW_POINTS', payload: total })
        await AsyncStorageStatic.setItem('newPoints', JSON.stringify(total))
    } catch (error) {
        console.log(error)
    }
}
export const ReferalPoints = (referral) => async dispatch => {
    const newUserId = JSON.parse(await AsyncStorageStatic.getItem('UserId'))
    var pointsdata = {
        "userId": newUserId,
        "points": 50,
        "referredBy": referral,
        "type": "forUser",
        "eventName": "Referral"
    }
    var config = {
        method: 'post',
        url: 'https://5c9twl6zfb.execute-api.ap-south-1.amazonaws.com/dev/api/v1/awarededPoints',
    };
    try {
        const { data } = await axios.post(config.url, pointsdata)
        // console.log(data, 'referalpoints')
        data && dispatch(getAwardPointsbyuserId(newUserId))
    } catch (error) {
        console.log(error)
    }

}