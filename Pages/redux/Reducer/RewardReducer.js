import { SET_REWARDS } from '../Actions/RewardsAction'

const initState = {
    rewards: null
}

export default function RewardReducer(state = initState, action) {
    const { type, payload } = action
    switch (type) {
        case SET_REWARDS: return { ...state, rewards: payload }
        default:
            return state

    }
}