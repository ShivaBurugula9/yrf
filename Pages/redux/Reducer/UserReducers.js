import {
    LOGIN, CHECK_LOGIN,
    GET_USER_BY_ID, ERRORS,
    LOGOUT
} from "../Actions/userAction";
import { HISTORY } from "../Actions/PointsAction";

const initState = {
    userFulldata: null,
    token: null,
    isLoading: true,
    userData: null,
    errors: null,
    newPoints: null,
    history: null,
}
export default function UserReducers(state = initState, action) {
    const { type, payload } = action
    switch (type) {
        case LOGIN:
            return {
                ...state,
                token: payload.token,
                isLoading: false,
                userFulldata: payload.data,
            }
        case GET_USER_BY_ID: return { ...state, userData: payload, isLoading: false }
        case CHECK_LOGIN:
            return { ...state, token: payload, isLoading: false }
        case LOGOUT: return { ...state, token: null, isLoading: false }
        case ERRORS: return { ...state, errors: payload }
        case HISTORY: return { ...state, history: payload }
        case 'REMOVE_ERROR':
            return { ...state, errors: null }
        case 'NEW_POINTS':
            return { ...state, newPoints: payload }
        default:
            return state
    }
}