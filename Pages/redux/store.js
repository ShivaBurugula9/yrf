import { createStore, applyMiddleware, combineReducers } from "redux";
import thunk from "redux-thunk";
import TicketsReducer from './Reducer/TicketsReducer'
import UserReducers from './Reducer/UserReducers'
import Reducer from "./Reducer/Reducer.js";
import RewardReducer from './Reducer/RewardReducer'
import loading from './Reducer/loaderreducer'
const rootReducer = combineReducers({
    Reducer,
    ticket: TicketsReducer,
    user: UserReducers,
    rewards: RewardReducer,
    loading
})

export const Store = createStore(rootReducer, applyMiddleware(thunk))