import React, { useEffect, useState } from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {
  View, Text, StyleSheet, TouchableOpacity,
  Image,  FlatList,
} from 'react-native';
import Share from 'react-native-share';
import Sheet from "../Pages/Components/Sheet";
import AsyncStorageStatic from '@react-native-community/async-storage'
import dynamicLinks from '@react-native-firebase/dynamic-links';
import { useDispatch, useSelector } from 'react-redux'
import { closeDrawer } from '../Pages/redux/Actions/action'
import { ReferalPoints } from './redux/Actions/PointsAction';
import { getTicketbyId, unclaimedTicketClaim } from './redux/Actions/TicketsAction';
import CustomLoader from './Components/CustomLoader';
import Coin from './Components/Coin';
import { COLORS, FONTS, SIZES } from './Constants/theme';
import { getUsersbyId } from './redux/Actions/userAction';
export default function Home({ navigation }) {
  const dispatch = useDispatch()
  const { show, } = useSelector(state => state.Reducer)
  const { rewards } = useSelector(state => state.rewards)
  const { userData, newPoints } = useSelector(state => state.user)
  const { sharedTicketwithId } = useSelector(state => state.ticket)
  useEffect(() => {
    setTimeout(() => {
      if (sharedTicketwithId?.id) {
        dispatch(ReferalPoints(sharedTicketwithId.id))
      } else if (sharedTicketwithId?.ticketId) {
        dispatch(getTicketbyId(sharedTicketwithId?.ticketId)),
         dispatch(unclaimedTicketClaim(sharedTicketwithId.userId))
        navigation.navigate('FromFileTicket')
      }
    }, 3000);
  }, [sharedTicketwithId])
  const renderItem = ({ item }) => (
    <TouchableOpacity style={styles.eachItem}
      onPress={() => navigation.navigate('RewardDetails', {
        itemDetail: item
      })}>
      <Image style={styles.smallImage} resizeMode='stretch' source={{ uri: item.bannerImage }} />
      <View style={{ top: 5 }}>
        <Text style={styles.innerImagefont2}>{`${item.rewardName.substring(0, 20)}`}</Text>
        <Coin points={item.points} />
      </View>
    </TouchableOpacity>
  )
  const myCustomShare = async () => {
    const userId = JSON.parse(await AsyncStorageStatic.getItem('UserId'))
    try {
      let shortLink = await dynamicLinks().buildLink({
        link: 'https://www.google.com?id=' + (userId),
        domainUriPrefix: 'https://gambt.page.link',
        android: {
          packageName: 'com.yrf'
        }
      })
      const shareOptions = {
        message: `${shortLink}`,
        // url : require('../assets/images/5star.jpg')
      }
      const ShareResponse = await Share.open(shareOptions)
      console.log(JSON.stringify(ShareResponse))
    } catch (errors) {
      console.log('Error => ', errors)
    }
  }
  const [loading, setloading] = useState(false);
  if (!userData) {
    return <CustomLoader />
  }
  const Referesh = async () => {
    let userId = JSON.parse(await AsyncStorageStatic.getItem('UserId'))
    dispatch(getUsersbyId(userId))
  }


  return (
    <View style={styles.body}>
      <Text style={styles.nameFont}>Hello {userData?.firstName}</Text>
      <LinearGradient source={require('../assets/images/Rectangle.png')}
        useAngle={true}
        angle={210} locations={[.29, 1.15]}
        colors={['#7A50DF', 'black']} style={styles.linearGradient}>
        <View style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
        }} >
          <View>
            <Text style={styles.buttonText}>
              Your Points
            </Text>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={{ fontSize: 33, color: '#ffff', fontWeight: 'bold', marginTop: 10 }}>
                {parseInt(newPoints)}
              </Text>
              <Text style={{ ...FONTS.font18, marginTop: 20, marginLeft: 10 }}>Pts</Text>
            </View>
          </View>
          <Image style={{ width: '55%', height: 95, left: 24 }} source={require('../assets/images/g.png')} />
        </View>
        <View style={{ flexDirection: 'row', top: 30, justifyContent: 'space-between', alignItems: 'center' }}>
          <TouchableOpacity onPress={() => navigation.navigate('Unclaimed')} style={styles.unclaimedwrapper}>
            <Text style={styles.buttonText2}>
              Unclaimed Points
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ alignSelf: 'center' }} onPress={() => navigation.navigate('History')}>
            <View style={styles.historyBtn}
            >
              <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 13, }}>History</Text>
            </View>
          </TouchableOpacity>
        </View>
      </LinearGradient>
      <View style={{
        flexDirection: 'row', marginTop: -10,
        justifyContent: 'space-between',
        width: SIZES.width - 40,
      }}>
        <Text style={styles.rewards}>Rewards</Text>
        <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => navigation.navigate('ViewAll')}>
          <Text style={styles.seeAll}>See All</Text>
        </TouchableOpacity>
      </View>
      <View style={{
        maxHeight: '51%',
        top: 12
      }}>
        <FlatList
          keyExtractor={item => item._id}
          data={rewards?.slice(0, 4)}
          horizontal
          showsHorizontalScrollIndicator={false}
          renderItem={renderItem}
          refreshing={loading}
          onRefresh={() => Referesh()}
        />
      </View>
      <Sheet show={show} onDismiss={() => {
        dispatch(closeDrawer());
      }} onPress={() => (dispatch(closeDrawer(), myCustomShare()))}
      />
    </View>
  );
}
const styles = StyleSheet.create({
  body: {
    flex: 1,
    backgroundColor: COLORS.background,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 8,
    paddingTop: 10
  },
  eachItem: {
    height: 240,
    margin: 7,
    width: SIZES.width * .45,
  },
  smallImage: {
    width: '100%',
    height: 200,
    borderRadius: 8,
    overflow: 'hidden',
  },
  notificationInput: {
    backgroundColor: '#272727',
    borderRadius: 10,
    height: 60,
    marginBottom: 20,
    textAlignVertical: 'top',
    color: '#fff',
    padding: 5,
    width: '102%'
  },
  nameFont: {
    ...FONTS.font18,
    fontSize: 20,
    left: 22,
    marginBottom: 6,
    marginRight: 'auto',
  },
  innerImagefont2: {
    ...FONTS.font15,
    left: 5
  },
  linearGradient: {
    padding: 15,
    borderRadius: 20,
    height: '32%',
    width: SIZES.width - 40,
    paddingLeft: 20,
    marginBottom: 13,
  },
  buttonText: {
    ...FONTS.font18,
    marginTop: 10,
    textAlign: 'center',
  },
  rewards: {
    ...FONTS.font18,
    fontSize: 21,
    marginLeft: 8,
    padding: 6,
  },
  seeAll: {
    ...FONTS.font18,
    color: '#BA9DFF',
    borderBottomColor: '#BA9DFF',
    borderBottomWidth: 1,
    right: 8
  },
  buttonText2: {
    fontSize: 12,
    color: '#FFFFFF',
    fontWeight: 'bold',
  },
  unclaimedwrapper: {
    backgroundColor: '#895741',
    borderRadius: 16,
    width: 125,
    height: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },
  historyBtn: {
    backgroundColor: '#1C1A29',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 54,
    height: 28,
    width: 80,
  },
  image: {
    width: 200,
    height: 200
  },
})

