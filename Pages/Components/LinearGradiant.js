import {  Text, View, TouchableOpacity } from 'react-native';
import React from 'react';
import { Dialog } from 'react-native-simple-dialogs';
import LinearGradient from 'react-native-linear-gradient';
import { FONTS } from '../Constants/theme';

const LinearGradiant = ({ dialogVisible, setdialogVisible, text, anyfuction }) => {
    return (
        <Dialog
            visible={dialogVisible}
            dialogStyle={{
                backgroundColor: 'transparent',
                height: 130,
            }}
            onTouchOutside={() => setdialogVisible(false)}>
            <LinearGradient
                useAngle={true}
                angle={210} locations={[.29, 1.15]}
                colors={['#7A50DF', 'black']} style={{
                    alignItems: 'center',
                    width: '100%', height: 150, borderRadius: 15, padding: 12
                }}>
                <Text style={FONTS.font18}>Congratulation</Text>
                <Text style={{ ...FONTS.font16, margin: 2, top: 5 }}>{text}</Text>
                <TouchableOpacity onPress={() => (setdialogVisible(false), anyfuction && anyfuction())}>
                    <View style={{
                        backgroundColor: 'black', borderRadius: 9, height: 40, width: 150,
                        alignItems: 'center', justifyContent: 'center', alignSelf: 'center', top: 20
                    }}
                    >
                        <Text style={{ ...FONTS.font18, fontSize: 20, }}>Okay</Text>
                    </View>
                </TouchableOpacity>
            </LinearGradient>
        </Dialog>
    );
};

export default LinearGradiant;

