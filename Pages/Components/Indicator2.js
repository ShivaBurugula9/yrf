import React, { useState } from 'react'
import { Text, View,  } from 'react-native'
import StepIndicator from 'react-native-step-indicator';
function Indicator2() {
  const [currentPosition, setcurrentPosition] = useState(3)
  const customStyles = {
    stepIndicatorSize: 8,
    currentStepIndicatorSize: 12,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: '#fe7013',
    stepStrokeWidth: 3,
    stepStrokeFinishedColor: '#7A50DF',
    stepStrokeUnFinishedColor: '#aaaaaa',
    separatorFinishedColor: '#7A50DF',
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: '#7A50DF',
    stepIndicatorUnFinishedColor: '#7A50DF',
    stepIndicatorCurrentColor: '#ffff',
    labelSize: 15,
  }

  
  return (
    <View >
      <StepIndicator
        customStyles={customStyles}
        currentPosition={currentPosition}
        stepCount={3}
        direction='vertical'
         />
    </View>
  )
}

export default Indicator2

