import * as React from 'react';
import { View, StyleSheet } from 'react-native';
import { Snackbar } from 'react-native-paper';

const CustomSnack = ({ success, snackVisible, setSnackVisible, snacktext }) => {

    const onDismissSnackBar = () => setSnackVisible(false);

    return (
        <View style={styles.container}>
            <Snackbar
                visible={snackVisible}
                duration={2000}
                style={{ backgroundColor: 'gray'}}
                onDismiss={onDismissSnackBar}
                // action={{
                //     label: 'OK',
                //     onPress: () => {
                //         onDismissSnackBar
                //     }
                // }}
                >
                {snacktext}
            </Snackbar>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',

    },
});

export default CustomSnack;