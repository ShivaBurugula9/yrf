import { View,ActivityIndicator } from 'react-native';
import React from 'react';
import { COLORS } from '../Constants/theme';
const CustomLoader = () => {
    return (
        <View style={{
            flex: 1,
            backgroundColor: COLORS.background,
            justifyContent: 'center',
            alignItems: 'center'
        }}>
          <ActivityIndicator size={55} color={COLORS.darkpurple} />
        </View>
    );
};

export default CustomLoader;
