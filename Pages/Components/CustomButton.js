import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import React from 'react';
import { COLORS } from '../Constants/theme';

const CustomButton = ({ onPress,btntext,disabled }) => {
    return (
        <TouchableOpacity disabled={disabled} onPress={() => onPress()}>
            <View style={styles.btn}
            >
                <Text style={{ color: 'white', fontWeight: '500', fontSize: 16 }}>{btntext}</Text>
            </View>
        </TouchableOpacity>
    );
};

export default CustomButton;

const styles = StyleSheet.create({
    btn: {
        backgroundColor: COLORS.secondary,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        height: 40,
        width: 78,
        marginTop: 5
    }
});
