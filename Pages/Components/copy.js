import { View, Text, ImageBackground, StyleSheet } from 'react-native';
import React from 'react';
import moment from 'moment';
import RewardImage from '../../assets/images/rewardImage.png'
import { FONTS, COLORS } from '../Constants/theme';

const HistorySingleComponent = ({ item }) => {
    return (
        <View style={styles.scroll}>
            <View style={{ width: '32%', height: 140,}}>
                {
                    item.rewardId || item.ticketId !== null && item.eventName !== 'Referral' ?
                        <ImageBackground resizeMode='stretch' style={styles.image} source={{ uri: item?.rewardId?.bannerImage || item?.ticketId?.ticketImage }} >
                            <View style={{ width: 100, height: 140 }}>
                            </View>
                        </ImageBackground>
                        :
                        <ImageBackground resizeMode='stretch' style={styles.image} source={RewardImage} >
                            <View style={{ width: 100, height: 140 }}>
                            </View>
                        </ImageBackground>
                }
            </View>
            <View style={{
                left: 7,
                width: '66%', height: '100%',
                flexDirection: 'row', padding: 2
            }}>
                <View style={{ minWidth: '80%', maxWidth: '80%' }}>
                    <Text style={{ ...FONTS.font18, color: COLORS.black }}>{item?.rewardId?.rewardName || item?.ticketId?.movieName}</Text>
                    {
                        item?.ticketId?.source && <Text style={styles.font}>Points earned via {item?.ticketId?.source || null}</Text>
                    }
                    <Text style={styles.font}>{moment(item.createdAt).format('llll')}</Text>
                    <Text style={{ color: "#F99601", fontSize: 14, marginTop: 10 }}>{item?.ticketId?.status}</Text>
                </View>
                <View style={{ width: '20%' }}>
                    <View style={{
                        alignItems: 'center', justifyContent: 'center',
                        backgroundColor: '#F99601',
                        borderRadius: 12, width: 44, height: 20, top: 2
                    }}>
                        <Text style={{ color: '#fff', fontSize: 11, fontWeight: '500' }}>{item.points || item?.ticketId?.price || '0'}</Text>
                    </View>
                </View>
            </View>
        </View>
    );
};
const styles = StyleSheet.create({
     image: {
        width: '100%',
        borderRadius: 12,
        overflow: 'hidden'
    },
    font: {
        ...FONTS.font15,
        color: 'black',
        top: 10,
    },
    moviewheader: {
        fontWeight: '500',
        color: 'black',
        fontSize: 16,
        marginBottom: 8,
        top: 1
    },
    scroll: {
        flexDirection: 'row',
        padding: 7,
        alignItems: 'center',
        bottom: 15,
        marginTop: 14,
        // backgroundColor:COLORS.purple
    }
})
export default HistorySingleComponent;
