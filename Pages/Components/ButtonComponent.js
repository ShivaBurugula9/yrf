import { StyleSheet} from 'react-native';
import React from 'react';
import { Button } from 'react-native-paper';
import { COLORS } from '../Constants/theme';

const ButtonComponent = ({style, icon, title, onPress,labelStyle }) => {
  return (
    <Button icon={icon} mode="contained" uppercase={false}
      style={{...style,backgroundColor: COLORS.secondary }}
      labelStyle={{...labelStyle,fontWeight:'500'}}
      contentStyle={{
        width: '100%',
        height: 50,
         alignItems: 'center'
      }}
      onPress={() => onPress()}>
      {title}
    </Button>
  );
};

export default ButtonComponent;

const styles = StyleSheet.create({});
