import messaging from '@react-native-firebase/messaging';

import AsyncStorageStatic from '@react-native-community/async-storage'

export async function requestUserPermission() {
    const authStatus = await messaging().requestPermission();
    const enabled =
        authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
        authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
        console.log('Authorization status:', authStatus);
        getfcmToken()
    }
}

const getfcmToken = async () => {
    let fcmToken = await AsyncStorageStatic.getItem('fcmToken')
    // console.log(fcmToken, 'the old token')
    if (!fcmToken) {
        try {
            const fcmToken = await messaging().getToken();
            if (fcmToken) {
                console.log(fcmToken, 'new')
                await AsyncStorageStatic.setItem('fcmToken', fcmToken)
            }
        } catch (error) {
            console.log(error,'errorNotification')
        }
    }
}

export const notificationListner = async () => {
    messaging().onNotificationOpenedApp(remoteMessage => {
        console.log(
          'Notification caused app to open from background state:',
          remoteMessage.notification,
        );
      });
       messaging()
      .getInitialNotification()
      .then(remoteMessage => {
        if (remoteMessage) {
          console.log(
            'Notification caused app to open from quit state:',
            remoteMessage.notification,
          );
        }
      });
}