/* eslint-disable prettier/prettier */
import React from 'react'
import moment from "moment";
import { Text, Dimensions, FlatList, View, ImageBackground, Image, StyleSheet } from 'react-native'

import { useSelector } from 'react-redux'
import { COLORS, FONTS } from '../Constants/theme';
const { width, height } = Dimensions.get('window')


function Referals() {

    const { history } = useSelector(state => state.user)
    let refData = history.filter((item) => item.eventName === 'Referral')
    const refDataPoints = refData.map(item => item.points)
    const reducer = (previousValue, currentValue) => previousValue + currentValue;
    let totalrefDataPoints = 0
    try {
        totalrefDataPoints = refDataPoints.reduce(reducer)
    } catch (error) {

    }

    const renderItem = ({ item }) => (
        <View style={styles.scroll}>
            <View style={{ width: '32%', height: 140 }}>
                {
                    item.rewardId || item.ticketId !== null && item.eventName !== 'Referral' ?
                        <ImageBackground resizeMode='cover' style={styles.image} source={{ uri: item?.rewardId?.bannerImage || item?.ticketId?.ticketImage }} >
                            <View style={{ width: 100, height: 140 }}>
                            </View>
                        </ImageBackground>
                        :
                        <ImageBackground resizeMode='cover' style={styles.image} source={require('../../assets/images/rewardImage.png')} >
                            <View style={{ width: 100, height: 140 }}>
                            </View>
                        </ImageBackground>
                }
            </View>
            <View style={{
                left: 7,
                width: '66%', height: '100%',
                flexDirection: 'row', padding: 2
            }}>
                <View style={{ width: '80%' }}>
                    <Text style={styles.moviewheader}>{item?.rewardId?.rewardName || item?.ticketId?.movieName}</Text>
                    <Text style={styles.font}>Earned via reference</Text>
                    <Text style={styles.font}>{moment(item.createdAt).format('llll')}</Text>
                </View>
                <View style={{ width: '20%' }}>
                    <View style={{
                        alignItems: 'center', justifyContent: 'center',
                        backgroundColor: '#F99601',
                        borderRadius: 12, width: 44, height: 20, top: 8
                    }}>
                        <Text style={{ color: '#fff', fontSize: 11, fontWeight: '500' }}>{item.points || item.price}</Text>
                    </View>
                </View>
            </View>
        </View>
    )
    return (
        <View style={styles.body}>
            <View style={{
                flexDirection: 'row',
                width: width - 80,
                justifyContent: 'space-between',
                alignSelf: 'center', marginTop: 10
            }}>
                <View style={{ alignItems: 'center' }}>
                    <Text style={{ color: '#F99601', fontWeight: '500', fontSize: 17 }}>Referral Pts</Text>
                    <Text style={{ color: '#F99601', top: 7, fontWeight: '500', fontSize: 17 }}>{totalrefDataPoints}</Text>
                </View>
                <View
                    style={{
                        borderLeftWidth: 1,
                        borderLeftColor: 'gray',
                        top: 9,
                        left: 10
                    }}
                />
                <View style={{ alignItems: 'center' }}>
                    <Text style={{ color: '#F99601', fontWeight: '500', fontSize: 17 }}>Referral Users</Text>
                    <Text style={{ color: '#F99601', top: 7, fontWeight: '500', fontSize: 17 }}>{refData.length}</Text>
                </View>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center', top: 10, marginHorizontal: 30, marginBottom: 10 }}>
                <View style={{ flex: 1, height: 1, backgroundColor: 'gray' }} />
            </View>
            <FlatList
                data={refData}
                renderItem={renderItem}
                keyExtractor={item => item._id}
            />
        </View>
    )
}
const styles = StyleSheet.create({
    body: {
        flex: 1,
        alignItems: 'center',
        height: height
    },
    image: {
        width: '100%',
        borderRadius: 20,
        overflow: 'hidden'
    },
    scroll: {
        flexDirection: 'row',
        padding: 18,
        alignItems: 'center'
    }, moviewheader: {
        fontWeight: '400',
        color: 'black',
        fontSize: 25,
    }, font: {
        ...FONTS.font16,
        color: COLORS.black
    },
})
export default Referals
