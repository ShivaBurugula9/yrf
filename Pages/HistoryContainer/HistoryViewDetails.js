/* eslint-disable prettier/prettier */
import React from 'react'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Allrewards from './Allrewards';
import Claimed from './Claimed';
import Referals from './Referals';
import { COLORS } from '../Constants/theme';

const Tab = createMaterialTopTabNavigator();

export default function HistoryViewDetails() {
    return (
        <Tab.Navigator
            screenOptions={({ route, focused }) => ({
                tabBarLabelStyle: {
                    fontSize: 15,
                    fontWeight: '500',
                },
                tabBarActiveTintColor: COLORS.darkpurple,
                tabBarInactiveTintColor: COLORS.darkpurple,
            })}
        >
            <Tab.Screen name="All" component={Allrewards} />
            <Tab.Screen name="Claimed" component={Claimed} />
            <Tab.Screen name="Referals" component={Referals} />
        </Tab.Navigator >
    );
}