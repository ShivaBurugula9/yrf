import React from 'react'
import {  View,  StyleSheet, FlatList } from 'react-native'
import { useSelector } from 'react-redux'
import HistorySingleComponent from '../Components/HistorySingleComponent';
import { SIZES } from '../Constants/theme';

function Allrewards() {
    const { history } = useSelector(state => state.user)
    let mapData = history.filter((item) => item.eventName !== 'UNCLAIMED_POINTS')
    const renderItem = ({ item }) => (
        <HistorySingleComponent item={item} />
    )
    return (
        <View style={styles.body}>
            <FlatList
                data={mapData}
                renderItem={renderItem}
                keyExtractor={item => item._id}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        padding: 8,
        width: SIZES.width,
        paddingTop: 18
    },
})

export default Allrewards
