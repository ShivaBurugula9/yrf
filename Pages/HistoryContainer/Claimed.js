import React, { useState } from 'react'
import {
    View, StyleSheet, TouchableOpacity,
    ImageBackground, FlatList, Text, Dimensions, ScrollView
} from 'react-native'
import { useSelector } from 'react-redux'
import LinearGradient from 'react-native-linear-gradient';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Clipboard from '@react-native-community/clipboard';
import { Dialog } from 'react-native-simple-dialogs';
import CustomSnack from '../Components/CustomSnack';
import { COLORS, FONTS, SIZES } from '../Constants/theme';

function Claimed() {
    const [snackVisible, setSnackVisible] = useState(false)
    const [dialogVisible1, setdialogVisible1] = useState(false)
    const { history } = useSelector(state => state.user)
    const [state, setstate] = useState(null)
    let mapData = history.filter((item) => !(item.ticketId && !item.rewardId) && item.eventName === ('POINTS_CLAIMED'))
    const renderItem = ({ item }) => (
        <TouchableOpacity onPress={() => (setdialogVisible1(true), setstate(
            item.rewardId
        ))} >
            <ImageBackground style={styles.bigImage} resizeMode='stretch' source={{ uri: item?.rewardId?.bannerImage }} >
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between', backgroundColor: 'rgba(0,0,0,0.6)', padding: 4
                }}>
                    <View>
                        <Text style={FONTS.font15}>{item?.rewardId?.rewardName}</Text>
                    </View>
                    <View style={{
                         alignItems: 'center', }}>
                        <Text style={FONTS.font15}>Claimed Pts</Text>
                        <Text style={FONTS.font15}>{parseInt(item.points) * -1}</Text>
                    </View>
                </View>
            </ImageBackground>
        </TouchableOpacity>
    )
    return (
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <FlatList
                data={mapData}
                renderItem={renderItem}
                keyExtractor={item => item._id}
            />
            <Dialog
                visible={dialogVisible1}
                dialogStyle={{
                    backgroundColor: 'transparent',
                    maxHeight: 380,
                    position:'absolute',
                    alignSelf:'center',
                    top:60,
                    zIndex:10
                }}
                onTouchOutside={() => setdialogVisible1(false)} >
                <LinearGradient
                    useAngle={true}
                    angle={210} locations={[.29, 1.15]}
                    colors={['#7A50DF', 'black']} style={styles.linearStyle}>
                    <Text style={{ color: '#fff', fontWeight: '500', fontSize: 20, margin: 6 }}>{state?.rewardName}</Text>
                    <View style={styles.codestyle}>
                        <Text style={{ color: '#fff', fontWeight: '500', fontSize: 20, margin: 6 }}>{state?.rewardCode}</Text>
                        <FontAwesome5 onPress={() => (Clipboard.setString(`${state?.rewardCode}`), setSnackVisible(true))} name="copy" size={30} color='#fff' />
                    </View>
                    <Text style={{ color: '#fff', fontWeight: '500', fontSize: 16, margin: 6 }}>Terms and Condition</Text>
                    <ScrollView>
                        {
                            state?.termsAndCondition.map((item, i) => (
                                <View key={i}>
                                    <Text style={{ color: 'yellow', fontSize: 15, fontWeight: '500' }}>{item.text}</Text>
                                </View>)
                            )
                        }
                    </ScrollView>
                    <TouchableOpacity  onPress={() => ((setdialogVisible1(false)))}>
                        <View style={styles.btn}
                        >
                            <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 20, }}>Okay</Text>
                        </View>
                    </TouchableOpacity>
                </LinearGradient>
                <CustomSnack style={{ position: 'absolute', bottom: 0, zIndex: 20, }} success={true} snackVisible={snackVisible} setSnackVisible={setSnackVisible} snacktext={'Code copied succefully'} />
            </Dialog>
        </View>
    )
}
const styles = StyleSheet.create({
    body: {
        flex: 1,
        alignItems: 'center',
        padding: 8,
    },
    font: {
        fontSize: 16,
        fontWeight: '400',
        color: 'black',
        top: 10,
        bottom: 10
    },
    bigImage: {
        height: 240,
        width:SIZES.width - 40,
        borderRadius: 12,
        overflow: 'hidden',
        justifyContent: 'flex-end',
        marginTop: 14
    },
    linearStyle: {
        alignItems: 'center',
        width: '100%',
        height: '100%',
        borderRadius: 15,
        padding: 12,
    },
    codestyle: {
        flexDirection: 'row',
        width: '80%',
        justifyContent: 'space-between',
        padding: 10,
        marginTop: 10,
        backgroundColor: COLORS.background,
        borderWidth: 2,
        borderStyle: 'dashed',
        borderColor: '#BA9DFF',
    },
    btn: {
        backgroundColor: 'black',
        borderRadius: 9,
        height: 40, width: 150,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
    }


})
export default Claimed
