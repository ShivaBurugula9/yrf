import React from 'react'
import { View, Text, Pressable, StyleSheet } from 'react-native'
import HomeContainer from './HomeContainer'
import Login from './Screens/Login'
import Register from './Screens/Register'
import RewardDetails from './Screens/RewardDetails';
import History from './Screens/History';
import Historydrawer from './Screens/Historydrawer';
import FromFileTicket from './Screens/FromFileTicket'
import ViewAll from './Screens/ViewAll';
import Welcome from './Screens/Welcome';
import Unclaimed from './Screens/Unclaimed';
import QrScanner from './QrContainer/Container';
import Opsscanned from './QrContainer/Opsscanned'
import HistoryViewDetails from './HistoryContainer/HistoryViewDetails';
import { openDrawer } from '../Pages/redux/Actions/action'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { useSelector, useDispatch } from 'react-redux';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { useNavigation } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Entypo from 'react-native-vector-icons/Entypo';
import CustomLoader from './Components/CustomLoader';
import { COLORS, SIZES } from './Constants/theme';


const Stack = createNativeStackNavigator();
export default function LoggedIn() {
  const { newPoints } = useSelector(state => state.user)
  const navigation = useNavigation();
  const dispatch = useDispatch()
  function HomeHeader() {
    return (
      <View style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center', width: SIZES.width / 2, right: 0, marginLeft: SIZES.width / 4
      }}>
        <View style={styles.points}>
          <Text style={{
            color: '#BA9DFF', fontSize: 13, fontWeight: '500'
          }} >{parseInt(newPoints)} Pts</Text>
        </View>
        <Pressable onPress={() => navigation.navigate('QrScanner')}>
          <Text>
            <Entypo name='upload' size={27} color='#fff' />
          </Text>
        </Pressable>
        <Pressable onPress={() => dispatch(openDrawer())}>
          <Text>
            <FontAwesome5 name='user-plus' size={24} color='#fff' />,
          </Text>
        </Pressable>
      </View>
    )
  }
  function HomeHeaderHistory() {
    return (
      <View>
        <Text style={{ color: '#ffff', fontSize: 20, fontWeight: 'bold' }}>
          History
        </Text>
      </View>
    )
  }
  const { token, isLoading } = useSelector(state => state.user)
  if (isLoading) {
    return <CustomLoader />
  }
  function HomeHeaderUnclaimed() {
    return (
      <View style={{ flexDirection: 'row', justifyContent: 'flex-end', width: SIZES.width - 80 }}>
        <Text style={{ margin: 12, left: -16 }}>
          <Pressable onPress={() => navigation.navigate('Home')} >
            <MaterialIcons name='menu' size={25} color='#fff' />
          </Pressable>
        </Text>
        <View style={{ flexDirection: 'row', justifyContent: 'flex-end', width: SIZES.width - 80 }}>
          <Text style={{ margin: 11 }}>
            <Pressable onPress={() => navigation.navigate('QrScanner')} >
              <Entypo name='upload' size={24} color='#fff' />
            </Pressable>
          </Text>
          <Pressable onPress={() => dispatch(openDrawer())}>
            <Text style={{ margin: 11 }}>
              <FontAwesome5 name='user-plus' size={24} color='#fff' />,
            </Text>
          </Pressable>
        </View>
      </View>
    )
  }
  return (
    <Stack.Navigator>
      {
        token === null ? (
          <>
            <Stack.Screen options={{
              headerShown: false
            }} name="Welcome" component={Welcome} />
            <Stack.Screen name="Register" options={{
              headerShown: false
            }} component={Register} />
            <Stack.Screen name="Login" options={{
              headerShown: false
            }} component={Login} />
            </>
        )
          : (
            <>
              <Stack.Screen name="Home" options={{
                headerShown: false
              }} component={HomeContainer} />
              <Stack.Screen name="RewardDetails" options={{
                headerTitle: (props) => <HomeHeader {...props} />,
                headerStyle: {
                  backgroundColor: COLORS.primary,
                }, headerTintColor: 'white',
              }} component={RewardDetails} />
              <Stack.Screen
                options={{
                  headerTitle: (props) => <HomeHeader {...props} />,
                  headerStyle: {
                    backgroundColor: COLORS.primary,
                  },
                  headerTintColor: 'white',
                }} name="ViewAll" component={ViewAll} />
              <Stack.Screen
                options={{
                  headerShown: false
                }} name="History" component={History} />
              <Stack.Screen
                options={{
                  headerTitle: (props) => <HomeHeaderHistory {...props} />,
                  headerTitleStyle: {
                    fontSize: 20,
                    alignSelf: 'center',
                    fontWeight: '500',
                    margin: 50, color: COLORS.background
                  },
                  headerTitleAlign: 'center',
                  headerStyle: {
                    backgroundColor: COLORS.background,
                  },
                  headerTintColor: 'white',
                }} name="HistoryViewDetails" component={HistoryViewDetails} />
              <Stack.Screen name="QrScanner" options={{
                headerTitleStyle: {
                  color: COLORS.background
                },
                headerTitleAlign: 'center',
                headerStyle: {
                  backgroundColor: COLORS.background,
                },
                headerTintColor: 'white',
              }} component={QrScanner} />
              <Stack.Screen name="FromFileTicket" options={{
                headerShown: false,
              }} component={FromFileTicket} />
              <Stack.Screen name="Reward History" options={{
                headerShown: false,
              }} component={Historydrawer} />
              <Stack.Screen name="Opsscanned" options={{
                headerTitleStyle: {
                  color: COLORS.background
                },
                headerStyle: {
                  backgroundColor: COLORS.background,
                },
                headerTintColor: 'white',
              }} component={Opsscanned} />
              <Stack.Screen name="Unclaimed"
                options={{
                  headerStyle: {
                    backgroundColor: COLORS.background,
                  },
                  headerTitle: (props) => <HomeHeaderUnclaimed {...props} />,
                }} component={Unclaimed} />
            </>
          )
      }

    </Stack.Navigator>
  )
}

const styles = StyleSheet.create({
  points: {
    minWidth: 70, right: 10,
    justifyContent: 'center', backgroundColor: COLORS.background,
    borderWidth: 1, borderColor: '#BA9DFF', padding: 3,
    paddingHorizontal: 6,
    borderRadius: 15, alignItems: 'center'
  }
})