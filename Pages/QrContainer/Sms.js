import React, { useRef, useState, useEffect } from 'react'
import { Text, View, TextInput, ScrollView } from 'react-native'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import * as yup from 'yup';
import { Formik } from 'formik'
import Clipboard from '@react-native-community/clipboard';
import { useDispatch, useSelector } from 'react-redux';
import { ticketDelete, UploadScanSmsTickets } from '../redux/Actions/TicketsAction';
import { useNavigation } from '@react-navigation/native';
import CustomSnack from '../Components/CustomSnack';
import { COLORS, FONTS, SIZES } from '../Constants/theme';
import ButtonComponent from '../Components/ButtonComponent';
import { styles } from './style';
function Sms() {
    const dispatch = useDispatch()
    const navigation = useNavigation();
    const { userData } = useSelector(state => state.user)
    const [first, setfirst] = useState(false);
    const [text, settext] = useState(false);
    const { errorofSms, error,fileTicket } = useSelector(state => state.ticket)
    const formikRef = useRef()
    const [snackVisible, setSnackVisible] = useState(false);
    const ValidationSchema = yup.object().shape({
        state: yup.string('must be string').
            required(' SMS must required'),
        language: yup.string('must be string').
            required(' Language must required'),
    });
    useEffect(() => {
        dispatch(ticketDelete())
    }, [])
    return (
        <View style={styles.body}>
            <Formik innerRef={formikRef}
                initialValues={{ state: '', language: '' }}
                // initialValues={{ state: 'Thank you for booking tickets on Paytm. Your ticket details for Zombivli are: February 3rd, 07:00 PM at Rahul Talkies 70MM, Shivaji.... Your seating information is GL-A06. Total amount: 220.20. Booking ID: 310820, Kiosk ID: 941523563255855603, Audi: SCREEN 2,  Secret Code: .. Please click here https://paytm.me/01F-cdf to get your order details. Here are all the precautions our cinema partners are taking https://paytm.com/offer/movies/cinema-reopening-guidelines/', language: 'Marathi' }}
                onSubmit={(values, { resetForm }) => (dispatch(UploadScanSmsTickets({ values, phoneNo: userData.phoneNo })), resetForm({ state: '', language: '' }))}
                validateOnMount={true}
                validationSchema={ValidationSchema}
            >
                {({ handleChange, handleSubmit, touched, errors, values }) => (
                    <ScrollView contentContainerStyle={styles.underBody}>
                        <View style={styles.textAreaContainer}>
                            <TextInput
                                style={styles.textArea}
                                value={values.state}
                                onChangeText={handleChange('state')}
                                underlineColorAndroid="transparent"
                                placeholder="Enter Link/SMS"
                                placeholderTextColor="#fff"
                                numberOfLines={8}
                                multiline={true}
                            />
                        </View>
                        {(errors.state && touched.state) &&
                            <View style={{ marginRight: 'auto', marginLeft: 30 }}>
                                <Text style={{ fontSize: 14, color: '#5A5A5A', fontWeight: '500', top: 15 }}>{errors.state}</Text>
                            </View>}
                        <View style={{
                            ...styles.textAreaContainer,
                            height: '10%', justifyContent: 'center',
                            padding: 8, marginTop: 20
                        }}>
                            <TextInput
                                style={styles.textArea2}
                                value={values.language}
                                onChangeText={handleChange('language')}
                                underlineColorAndroid="transparent"
                                placeholder="Enter Movie Language"
                                placeholderTextColor="#fff"
                            />
                        </View>
                        {(errors.language && touched.language) &&
                            <View style={{
                                marginLeft: 30,
                                marginRight: 'auto', top: 15
                            }}>
                                <Text style={FONTS.h5}>{errors.language}</Text>
                            </View>}
                        <View style={{ top: 30 }}>
                            <ButtonComponent onPress={() => (handleSubmit(), (!errors.language && !errors.state) && navigation.navigate('FromFileTicket'))}
                                title={'Send Sms'} labelStyle={{ fontSize: 19, fontWeight: '500' }} />
                        </View>
                        <View style={{ alignItems: 'center', marginTop: 60 }}>
                            <Text style={{
                                color: '#fff', fontSize: 20
                            }}>Or</Text>
                            <Text style={{
                                color: '#fff', fontSize: 18
                            }}>Forward Ticket details on</Text>
                            <View >
                                <View onPress style={styles.searchSection}>
                                    <Text style={styles.input} >
                                        +8705617795
                                    </Text>
                                    <FontAwesome5 onPress={() => (Clipboard.setString('+8705617795'), setSnackVisible(true),
                                        setfirst(true), settext('copied successfully'))}
                                        style={styles.searchIcon} name="copy" size={18} color='#fff' />
                                </View>
                                <View onPress style={styles.searchSection}>
                                    <Text style={{ ...styles.input, minWidth: SIZES.width * .5, }} >
                                        test@parse.nirmitee.io
                                    </Text>
                                    <FontAwesome5 onPress={() => (Clipboard.setString('test@parse.nirmitee.io'),
                                        setSnackVisible(true), setfirst(true), settext('copied successfully'))}
                                        style={styles.searchIcon} name="copy" size={18} color='#fff' />
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                )}
            </Formik>
            <CustomSnack success={first} snackVisible={snackVisible} setSnackVisible={setSnackVisible} snacktext={text} />
        </View >
    )
}


export default Sms

