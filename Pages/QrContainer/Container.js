import React, { useEffect, useState } from 'react';
import { BottomNavigation } from 'react-native-paper';
import { View } from 'react-native';
import { changeError, imageUpload } from '../redux/Actions/FileuploadAction';
import { useDispatch, useSelector } from 'react-redux';
import ImagePicker from 'react-native-image-crop-picker';
import Sms from './Sms'
import { useNavigation } from '@react-navigation/native';
import CustomSnack from '../Components/CustomSnack';
import { COLORS } from '../Constants/theme';
import ButtonComponent from '../Components/ButtonComponent';
import { ticketDelete } from '../redux/Actions/TicketsAction';
const Choose = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch()
  const { userData } = useSelector(state => state.user)
  const { error,errorofSms } = useSelector(state => state.ticket)
  const [snackVisible, setSnackVisible] = useState(false);
  // console.log(error,errorofSms,'container')
  const ChossefromFile = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
    }).then(image => {
      dispatch(imageUpload({ image: image, phoneNo: userData.phoneNo }))
      navigation.navigate('FromFileTicket')
    });
  };
  useEffect(()=>{
    dispatch(ticketDelete())
  },[])
  return (
    <View style={{ flex: 1, alignItems: 'center' }}>
      <View style={{
        flex: 1, backgroundColor: 'gray',
        marginTop: '50%', justifyContent: 'center',
        paddingTop: 40, padding: 20, borderRadius: 13
      }}>
        <ButtonComponent onPress={() => ChossefromFile()} icon={'upload'}
          title={'Upload Image'} labelStyle={{ fontSize: 19, fontWeight: '500' }} />
      </View>
      <CustomSnack snackVisible={snackVisible} setSnackVisible={setSnackVisible} snacktext={'Please try again'} />
    </View>
  );
};
const Container = () => {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {
      key: 'choose', title: 'File',
      icon: 'image-plus'
    },
    {
      key: 'SMS', title: 'Sms',
      icon: 'card-text-outline',
    },
  ]);

  const renderScene = BottomNavigation.SceneMap({
    choose: Choose,
    SMS: Sms,
  });

  return (
    <BottomNavigation
      navigationState={{ index, routes }}
      onIndexChange={setIndex}
      renderScene={renderScene}
      activeColor={COLORS.white}
      barStyle={{ backgroundColor: COLORS.darkpurple }}
    />
  );
};

export default Container;