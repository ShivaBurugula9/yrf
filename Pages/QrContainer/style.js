import { StyleSheet } from "react-native"
import { SIZES, COLORS } from "../Constants/theme"
export const styles = StyleSheet.create({
    body: {
        flex: 1,
        alignItems: 'center',
    },
    underBody: {
        alignItems: 'center',
        width: SIZES.width,
        backgroundColor:  COLORS.background
    },
    textAreaContainer: {
        borderWidth: 1,
        width: SIZES.width*.9,
        height: '30%',
        backgroundColor: COLORS.gray,
        padding: 8,
        fontSize: 15,
        borderRadius: 8,
        top:5,
        overflow:'hidden'
    },
    textArea: {
        height:220,
        color: COLORS.white,
        justifyContent: "flex-start",
        textAlignVertical: 'top',
        fontSize: 18
    },
    textArea2: {
        height: 50,
        color: COLORS.white,
        fontSize: 15,
    },
    input: {
        paddingTop: 2,
        fontSize: 16,
        paddingLeft: 7,
        paddingBottom: 4,
        color: COLORS.white
    },
    searchSection: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: COLORS.gray,
        width: SIZES.width * .6,
        borderRadius: 8,
        margin: 4,
        padding: 4,
    },
    searchIcon: {
        right: 10
    }
})