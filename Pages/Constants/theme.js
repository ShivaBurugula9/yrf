import { Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");

export const COLORS = {
    // base colors
    primary: '#38354B',
    secondary: '#1A2C50',
    background: '#201E2D',
    // colors
    error: '#5A5A5A',
    black: "#000000",
    white: "#FFFFFF",
    gray: 'gray',
    purple: '#BA9DFF',
    darkpurple: '#7A50DF',
    //others
    transparent: "transparent",
    darkgray: '#898C95',
};

export const SIZES = {
    // global sizes
    base: 8,
    font: 14,
    radius: 30,
    padding: 10,
    padding2: 12,

    // font sizes
    largeTitle: 50,
    h1: 30,
    h2: 22,
    h3: 20,
    h4: 18,
    h5: 14,
    body1: 30,
    body2: 20,
    body3: 15,
    body4: 14,
    body5: 12,
    body6: 16,
    body7: 17,
    body8: 18,

    // app dimensions
    width,
    height
};

export const FONTS = {
    largeTitle: { fontFamily: "Roboto-regular", fontSize: SIZES.largeTitle, lineHeight: 55 },
    h1: { fontFamily: "Roboto-Black", fontSize: SIZES.h1, lineHeight: 36 },
    h2: { fontFamily: "Roboto-Bold", fontSize: SIZES.h2, color: COLORS.white, fontWeight: '600' },
    h3: { fontFamily: "Roboto-Bold", fontSize: SIZES.h3, lineHeight: 22 },
    h4: { fontFamily: "Roboto-Bold", fontSize: SIZES.h4, lineHeight: 22 },
    h5: { fontFamily: "Roboto-Bold", fontSize: SIZES.h5, color: COLORS.error, fontWeight: 'bold' },
    font15: { fontFamily: "Roboto-regular", fontSize: SIZES.body3, color: COLORS.white, fontWeight: '700' },
    font16: { fontFamily: "Roboto-regular", fontSize: SIZES.body6, color: COLORS.white, fontWeight: '700' },
    font17: { fontFamily: "Roboto-regular", fontSize: SIZES.body7, color: COLORS.white, fontWeight: '700' },
    font18: { fontFamily: "Roboto-regular", fontSize: SIZES.body8, color: COLORS.white, fontWeight: '700' },
};

const appTheme = { COLORS, SIZES, FONTS };

export default appTheme;