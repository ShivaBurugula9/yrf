import {StyleSheet} from 'react-native'
import {COLORS,FONTS,SIZES} from '../../Constants/theme'

export const styles = StyleSheet.create({
    body: {
      flex: 1,
      backgroundColor: COLORS.background,
      justifyContent: 'center',
      height: SIZES.height,
      padding: 18,
    },
    topWrapper: {
      justifyContent: 'center',
      alignItems: 'center',
    },
    lowerWrapper: {
      justifyContent: 'flex-start',
      alignItems: 'center',
      padding: 10,
      flex: 1,
    },
    sheetBtn: {
      backgroundColor: COLORS.primary,
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 11,
      height: 40,
      width: 140,
      marginTop: 20
    },
    fonts: {
      ...FONTS.font18,
      fontSize: 30,
      alignSelf: 'center',
      marginBottom: 65
    },
    btn: {
      alignSelf: 'center',
      height: 55,
      width: SIZES.width - 60,
    },
    searchSection: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      backgroundColor: COLORS.secondary,
      width: SIZES.width - 40,
      borderRadius: 8,
      marginTop: 15,
      padding: 6,
    },
    searchIcon: {
      padding: 10,
      fontSize: 20
    },
    input: {
      paddingTop: 8,
      fontSize: 20,
      paddingLeft: 8,
      paddingBottom: 8,
      color: '#ffff',
      width: '90%'
    },
  })