import React, { useState } from 'react';
import { View, Text, TouchableOpacity, TextInput, ScrollView, } from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { removeError, UserLogin } from '../../redux/Actions/userAction';
import { useDispatch, useSelector } from 'react-redux';
import { Dialog } from 'react-native-simple-dialogs';
import * as yup from 'yup';
import { Formik } from 'formik'
import LinearGradient from 'react-native-linear-gradient';
import { SIZES, FONTS } from '../../Constants/theme';
import { Button } from 'react-native-paper';
import { styles } from './styles'
export default function Login({ navigation }) {
  const { errors } = useSelector(state => state.user)
  const ValidationSchema = yup.object().shape({
    phoneNo: yup.string().
      min(10, ({ min }) => `Phone number must be ${min} digits`).
      max(10, 'Phone number must be 10 digits').
      required('Phone number must required'),
    email: yup.string().email('Please enter valid email')
      .required('Email must required'),
    password: yup.string().
      min(4, ({ min }) => `Password must be atleast ${min} characters`)
      .required(' Password must required'),
  });
  const dispatch = useDispatch()
  const [showPassword, setshowPassword] = useState(true)
  return (
    <>
      <Formik
        initialValues={{ email: '', phoneNo: '', password: '' }}
        // initialValues={{ email: 'jit2@gmail.com', phoneNo: '0000000001', password: 'Test' }}
        // initialValues={{ email: 'jit2@gmail.com', phoneNo: '9939124667', password: 'Test' }}
        onSubmit={values => dispatch(UserLogin(values))}
        validateOnMount={true}
        validationSchema={ValidationSchema}
      >
        {({ handleChange, handleSubmit, touched, errors, values }) => (
          <ScrollView >
            <View style={styles.body}>
              <View style={styles.topWrapper}>
                <Text style={styles.fonts}>Welcome</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', width: SIZES.width }}>
                  <TouchableOpacity >
                    <Text style={{ fontSize: 20, ...FONTS.font18, marginLeft: 30, bottom: 3 }}>Login </Text>
                  </TouchableOpacity >
                  <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                    <Text style={FONTS.font18}>/Sign up </Text>
                  </TouchableOpacity>
                </View>
                <View style={{ alignItems: 'center' }}>
                  <View style={styles.searchSection}>
                    <TextInput
                      style={styles.input}
                      keyboardType='numeric'
                      placeholder='Phone Number'
                      placeholderTextColor='#5A5A5A'
                      onChangeText={handleChange('phoneNo')}
                      value={values.phoneNo}
                      underlineColorAndroid="transparent"
                    />
                  </View>
                  {(errors.phoneNo && touched.phoneNo) &&
                    <View style={{ marginRight: 'auto', left: 10 }}>
                      <Text style={FONTS.h5}>{errors.phoneNo}</Text>
                    </View>}
                  <View style={styles.searchSection}>
                    <TextInput
                      style={styles.input}
                      placeholder='Email'
                      keyboardType='email-address'
                      placeholderTextColor='#5A5A5A'
                      onChangeText={handleChange('email')}
                      value={values.email}
                      underlineColorAndroid="transparent"
                    />
                  </View>
                  {(errors.email && touched.email) &&
                    <View style={{ marginRight: 'auto', left: 10 }}>
                      <Text style={FONTS.h5}>{errors.email}</Text>
                    </View>
                  }
                  <View style={styles.searchSection}>
                    <TextInput
                      style={styles.input}
                      placeholder='Password'
                      placeholderTextColor='#5A5A5A'
                      onChangeText={handleChange('password')}
                      value={values.password}
                      secureTextEntry={showPassword}
                      underlineColorAndroid="transparent"
                    />
                    <FontAwesome5 onPress={() => setshowPassword(!showPassword)} style={styles.searchIcon} name={!showPassword ? "eye" : "eye-slash"} size={40} color='#5A5A5A' />
                  </View>
                  {(errors.password && touched.password) &&
                    <View style={{ marginRight: 'auto', left: 10 }}>
                      <Text style={FONTS.h5}>{errors.password}</Text>
                    </View>
                  }
                </View>
              </View>
              <View style={styles.lowerWrapper}>
                <Button uppercase={false}
                  labelStyle={{
                    fontSize: 19,
                    fontWeight: '500'
                  }}
                  color='#38354B'
                  style={{ marginTop: 20, borderRadius: 12 }}
                  contentStyle={styles.btn}
                  mode="contained" onPress={() => handleSubmit()}>
                  Login
                </Button>
              </View>
            </View>
          </ScrollView>
        )}
      </Formik>
      <Dialog
        visible={Boolean(errors)}
        dialogStyle={
          {
            borderRadius: 12,
            height: 250,
            alignItems: 'center', backgroundColor: 'transparent'
          }
        } >
        <LinearGradient
          useAngle={true}
          angle={210} locations={[.29, 1.15]}
          colors={['#7A50DF', 'black']} style={{
            alignItems: 'center',
            width: '100%', height: 'auto', borderRadius: 15, padding: 12
          }}>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ fontSize: 19, color: 'white', fontWeight: '500', marginTop: 10 }}>{errors}</Text>
            <TouchableOpacity onPress={() => dispatch(removeError())} style={styles.sheetBtn}>
              <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 18 }}>Retry</Text>
            </TouchableOpacity>
          </View>
        </LinearGradient>
      </Dialog>
    </>
  );
}


