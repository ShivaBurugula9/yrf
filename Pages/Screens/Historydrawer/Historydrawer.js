
import React, { useEffect, useState } from 'react'
import {
    View, FlatList, Text, StyleSheet,
    TouchableOpacity,
} from 'react-native';
import axios from 'axios';
import { Caption } from "react-native-paper";
import AntDesign from 'react-native-vector-icons/AntDesign';
import { useSelector } from 'react-redux'
import CustomLoader from '../../Components/CustomLoader';
import { COLORS, SIZES, FONTS } from '../../Constants/theme';
import HistorySingleComponent from '../../Components/HistorySingleComponent';
function HistoryDrawer({ navigation }) {

    const [isLoading, setisLoading] = useState(true)
    const { history, newPoints } = useSelector(state => state.user)
    let mapData = history.filter((item) => item.eventName !== 'UNCLAIMED_POINTS')
    let refData = history.filter((item) => item.eventName === 'Referral')
    const data = mapData.map(item => item.ticketId?.price || item.points).filter(item => item > 0)
    const refDataPoints = refData.map(item => item.points)
    const reducer = (previousValue, currentValue) => previousValue + currentValue;
    let totalrefDataPoints = 0
    try {
        totalrefDataPoints = refDataPoints.reduce(reducer)
    } catch (error) {

    }
    const setImage = async (data) => {
        if (data?.movieName) {
            const results = await axios.get(`https://imdb-api.com/en/API/Search/k_8qg681nx/${data.movieName}`)
            if (results) {
                const resultImage = results.data.results[0].image
                const result = await axios.put(`https://5c9twl6zfb.execute-api.ap-south-1.amazonaws.com/dev/api/v1/tickets/${data._id}`, { ticketImage: resultImage })
                if (result.data) {
                    console.log(result.data, '1')
                }
            }
        }
        return
    }
    useEffect(() => {
        const imageCheck = mapData?.filter(item => item.ticketId).map(item => item.ticketId)
        const imagehistory = imageCheck?.filter(item => !item.ticketImage)
        imagehistory?.map(item => setImage(item))
    }, [])
    useEffect(() => {
        setTimeout(() => {
            setisLoading(false)
        }, 2000);
    }, [isLoading])
    if (isLoading) {
        return <CustomLoader />
    }
    const renderItem = ({ item }) => (
        <HistorySingleComponent item={item} />
    )


    return (
        <View style={styles.body}>
            <View style={styles.headerStyle}>
                <TouchableOpacity onPress={() => navigation.navigate('HomePage')} >
                    <AntDesign name='arrowleft' size={25} color='#fff' />
                </TouchableOpacity>
                <Text style={{ ...FONTS.font18, fontSize: 22, paddingRight: '40%' }}>
                    History
                </Text>
            </View>
            <View style={{ width: SIZES.width, padding: 12 }}>
                <View style={styles.box}>
                    <Caption style={{ ...FONTS.font18, fontSize: 21 }}>Last Earned</Caption>
                    <Text style={{ ...FONTS.font18, fontSize: 21 }}>{data[0] || '0'} Pts</Text>
                </View>
                <View style={styles.box}>
                    <Caption style={{ ...FONTS.font18, fontSize: 21 }}>Referal earned</Caption>
                    <Text style={{ ...FONTS.font18, fontSize: 21 }}>{totalrefDataPoints || 0} Pts</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', top: 10, marginBottom: 10 }}>
                    <View style={{ flex: 1, height: 1, backgroundColor: '#ADADB2' }} />
                </View>
                <View style={styles.box}>
                    <Caption style={{
                        ...FONTS.font18,
                        color: COLORS.purple, fontSize: 21,
                    }}>Total Points</Caption>
                    <Text style={{
                        ...FONTS.font18,
                        color: COLORS.purple, fontSize: 21,
                    }}>{parseInt(newPoints)} Pts</Text>
                </View>
            </View>
            <View style={styles.lowerWrapper}>
                <View style={{
                    padding: 10, marginLeft: 'auto', right: 8
                }}>
                    <TouchableOpacity onPress={() => navigation.navigate('HistoryViewDetails')}>
                        <Text style={{ ...FONTS.font18, color: COLORS.darkpurple }}>View Details</Text>
                    </TouchableOpacity>
                </View>
                <FlatList
                    data={mapData}
                    initialNumToRender={10}
                    renderItem={renderItem}
                    keyExtractor={item => item._id}
                />
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    body: {
        flex: 1,
        backgroundColor: COLORS.background,
        alignItems: 'center',
        height: SIZES.height
    },
    box: {
        alignItems: 'center',
        padding: 8,
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginTop: 10
    },
    lowerWrapper: {
        width: SIZES.width,
        minHeight: '70%',
        maxHeight: '70%',
        backgroundColor: '#ffff',
        borderTopRightRadius: 25,
        borderTopLeftRadius: 25,
        paddingBottom: 55,
        padding: 6
    }, headerStyle: {
        flexDirection: 'row',
        top: 14,
        marginBottom: 14,
        justifyContent: 'space-between',
        width: SIZES.width * .95,
        alignItems: 'center',
    }
})
export default HistoryDrawer





