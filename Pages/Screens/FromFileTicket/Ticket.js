import { View, Text, StyleSheet, TouchableOpacity, Image, ActivityIndicator, } from 'react-native'
import RBSheet from "react-native-raw-bottom-sheet";
import Ionicons from 'react-native-vector-icons/Ionicons';
import Share from 'react-native-share';
import moment from "moment";
import { Button } from "react-native-paper";
import dynamicLinks from '@react-native-firebase/dynamic-links';
import AsyncStorageStatic from '@react-native-community/async-storage'
import { Dialog } from 'react-native-simple-dialogs';
import { useSelector, useDispatch } from 'react-redux'
import { useNavigation } from '@react-navigation/native';
import { getTicketStatus, ticketDelete } from "../../redux/Actions/TicketsAction";
import { COLORS, FONTS, SIZES } from "../../Constants/theme";
import React, { useState, useRef, useEffect } from 'react';
import { styles } from './styles';

const Ticket = () => {

    const { fileTicket } = useSelector(state => state.ticket)
    // console.log(fileTicket, 'tickets')
    // const fileTicket = {
    //     "__v": 0, "_id": "61fa5a238e25e5000920a860", "address": "Inox Cinema, Ayodhya Road, Lucknow, Uttar Pradesh, India", "bookingDate": "2022-02-03 00:00:00", "bookingId": "PMDX0008170691", "bookingTime": "11:15 AM", "city": "Lucknow", "count": "1", "createdAt": "2022-02-02T10:17:08.006Z",
    //     "deleted": false, "deletedAt": null, "deletedBy": null, "eventName": null, "id": "61fa5a238e25e5000920a860", "isActve": false, "locale": "Hindi",
    //     "movieName": "Pushpa: The Rise - Part 01",
    //     "phoneNo": "0000000001", "platform": "paytm",
    //     "scanDate": "2022-02-02T10:17:07.772Z",
    //     "screenType": "2D", "source": "scanned",
    //     "state": "Uttar Pradesh", "status": "Uploaded",
    //     "theater": "INOX Lucknow Crown Mall, Lucknow",
    //     "ticketImage": "",
    //     "ticketNumbers": ["B3"], "updatedAt": "2022-02-02T10:17:11.785Z", "userId": "61f778c5d37ddb000953f2ba"
    // }
    // const fileTicket = { "__v": 0, "_id": "61fa5a238e25e5000920a860", "address": "Inox Cinema, Ayodhya Road, Lucknow, Uttar Pradesh, India", "bookingDate": "2022-02-03 00:00:00", "bookingId": "PMDX0008170691", "bookingTime": "11:15 AM", "city": "Lucknow", "count": "1", "createdAt": "2022-02-02T10:17:08.006Z", "deleted": false, "deletedAt": null, "deletedBy": null, "eventName": null, "id": "61fa5a238e25e5000920a860", "isActve": false, "locale": "Hindi", "movieName": "Pushpa: The Rise - Part 01", "phoneNo": "0000000001", "platform": "paytm", "scanDate": "2022-02-02T10:17:07.772Z", "screenType": "2D", "source": "scanned", "state": "Uttar Pradesh", "status": "Uploaded", "theater": "INOX Lucknow Crown Mall, Lucknow", "ticketImage": "https://imdb-api.com/images/original/MV5BMmQ4YmM3NjgtNTExNC00ZTZhLWEwZTctYjdhOWI4ZWFlMDk2XkEyXkFqcGdeQXVyMTI1NDEyNTM5._V1_Ratio0.7273_AL_.jpg", "ticketNumbers": ["B3"], "updatedAt": "2022-02-02T10:17:11.785Z", "userId": "61f778c5d37ddb000953f2ba" }
    const { userData } = useSelector(state => state.user)
    const dispatch = useDispatch()
    const navigation = useNavigation()
    const [dialogVisible, setdialogVisible] = useState(false)
    const [loading, setloading] = useState(false);
    const [loading1, setloading1] = useState(false);
    const [loading3, setloading3] = useState(false);
    const refRBSheet = useRef();
    const myCustomShare = async (ticketId) => {
        const userId = JSON.parse(await AsyncStorageStatic.getItem('UserId'))
        try {
            let shortLink = await dynamicLinks().buildLink({
                link: `https://www.google.com?ticketId=${ticketId}&userId=${userId}`,
                domainUriPrefix: 'https://gambt.page.link',
                android: {
                    packageName: 'com.yrf'
                }
            })
            const shareOptions = {
                message: `${shortLink}`,
                // url : files.image
            }
            const ShareResponse = await Share.open(shareOptions)
            console.log(JSON.stringify(ShareResponse))
        } catch (error) {
            console.log('Error => ', error)
        }
    }
    const claim = () => {
        dispatch(getTicketStatus({ ...fileTicket, ticketId: fileTicket._id }))
    }
    const ticketsseat = fileTicket?.ticketNumbers.join(',')
    const newDate = moment(fileTicket?.bookingDate).format('MMMM Do YYYY');
    return (
        <View style={{
            alignItems: 'center',
            flex: 1, width: '98%'
        }}>
            <View style={styles.body} >
                <Text style={{
                    ...FONTS.font15, fontSize: 22,
                }}>
                    Ticket Summary
                </Text>
                <TouchableOpacity onPress={() => (navigation.goBack(), dispatch(ticketDelete()))}>
                    <Ionicons name='close' style={{ color: '#ffff', fontSize: 35, fontWeight: 'bold' }} />
                </TouchableOpacity>
            </View>
            {
                fileTicket &&
                <View style={styles.upperContainer}>
                    <View style={{ flexDirection: 'row', padding: 2 }}>
                        {
                            fileTicket.ticketImage !== 'image-url' ?
                             <Image resizeMode="stretch" style={styles.square} source={{ uri: fileTicket.ticketImage }} />
                                : <View style={{
                                    ...styles.square, backgroundColor: 'gray',
                                    alignItems: 'center', justifyContent: 'center'
                                }}>
                                    <Text style={FONTS.font17}>Movie Poster</Text>
                                </View>
                        }

                        <View style={{ left: 10, width: '65%', overflow: 'hidden' }} >
                            <Text style={styles.font}>{`${fileTicket?.movieName.substring(0, 30)}`}</Text>
                            <Text style={styles.font1}>{`${fileTicket?.theater.substring(0, 30)}`}</Text>
                            <Text style={styles.font2}>{newDate}  {fileTicket.bookingTime}</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', top: 10 }}>
                        <View style={{ width: 45, height: 45, backgroundColor: COLORS.background, borderRadius: 24 }}></View>
                        <Image style={{ width: SIZES.width - 85 }} source={require('../../../assets/images/Vector9.png')} />
                        <View style={{ width: 45, height: 45, backgroundColor: COLORS.background, borderRadius: 24 }}></View>
                    </View>
                    <View style={{ width: SIZES.width - 20, padding: 17 }} >
                        <View>
                            <Text style={styles.lowerHeading}>Booking ID</Text>
                            <Text style={styles.value}>{fileTicket?.bookingId}</Text>
                        </View>
                        <View>
                            <Text style={styles.lowerHeading}>QNTY</Text>
                            <Text style={styles.value}>{fileTicket?.count}</Text>
                        </View>
                        <View >
                            <Text style={styles.lowerHeading}>Seat</Text>
                            <Text style={styles.value}>{ticketsseat} </Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center', top: 10, marginBottom: 10 }}>
                                <View style={{ flex: 1, height: 1, backgroundColor: '#ADADB2' }} />
                            </View>
                        </View>
                        <View >
                            <Text style={styles.lowerHeading}>Points Earned</Text>
                            <Text style={styles.value, {
                                ...FONTS.font15,
                                fontSize: 23,
                            }}>To be verified</Text>
                        </View>
                    </View>
                </View>
            }
            <Dialog
                visible={dialogVisible}
                dialogStyle={{
                    backgroundColor: '#1C1A29',
                    borderRadius: 12, height: 330,
                    alignItems: 'center', justifyContent: 'center'
                }}
                onTouchOutside={() => setdialogVisible(false)} >
                <Text style={{
                    ...FONTS.font15, fontSize: 26, margin: 6
                }}>Hello   {userData?.firstName} </Text>
                <Text style={{
                    ...FONTS.font15, fontSize: 20, margin: 6
                }}>You have {fileTicket?.count - 1} more tickets in</Text>
                <Text style={{
                    ...FONTS.font15, fontSize: 20, margin: 6
                }}>your wallet. Refer to your,</Text>
                <Text style={{
                    ...FONTS.font15, fontSize: 20, margin: 6
                }}>friends RIGHT NOW to get</Text>
                <Text style={{
                    ...FONTS.font15, fontSize: 20, margin: 6
                }}>max points !</Text>
                <TouchableOpacity onPress={() => (setdialogVisible(false), navigation.navigate('HomePage'))}>
                    <View style={{
                        backgroundColor: 'black', borderRadius: 9, height: 40, width: 150,
                        alignItems: 'center', justifyContent: 'center', alignSelf: 'center', top: 20
                    }}
                    >
                        <Text style={{
                            ...FONTS.font15, fontSize: 20,
                        }}>Okay</Text>
                    </View>
                </TouchableOpacity>
            </Dialog>
            <Button uppercase={false}
                style={styles.btn}
                loading={loading3}
                labelStyle={{ fontSize: 19, fontWeight: '500' }}
                contentStyle={{
                    backgroundColor: COLORS.primary, alignSelf: 'center'
                }}
                mode="contained"
                onPress={() =>
                (setTimeout(() => {
                    (refRBSheet.current.open())
                    setloading3(false)
                }, 3000), setloading3(true), claim())}
            >
                Claim Points
            </Button>
            <RBSheet
                ref={refRBSheet}
                customStyles={{
                    wrapper: {
                        backgroundColor: "transparent",
                    },
                    draggableIcon: {
                        backgroundColor: "#000"
                    },
                    container: {
                        borderTopRightRadius: 20,
                        borderTopLeftRadius: 20,
                        alignItems: 'center',
                        height: '45%',
                        padding: 8,
                    }
                }}
            >
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Image style={{ width: '35%', height: 140 }} resizeMode='cover' source={require('../../../assets/images/image11.png')} />
                    <View>
                        <Text style={styles.sheetfont1}>Points will be available after</Text>
                        <Text style={styles.sheetfont2}>6 hours</Text>
                    </View>
                </View>
                {
                    fileTicket?.count == 1 ?
                        <>
                            <Text style={styles.sheetfont3}>You claimed points for</Text>
                            <Text style={styles.sheetfont3}>the ticket</Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <TouchableOpacity style={{ alignSelf: 'center', margin: 12 }} onPress={() => navigation.navigate('History')}>
                                    <View style={styles.sheetBtn1}>
                                        <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 18 }}>View history</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </> :
                        <>
                            <Text style={styles.sheetfont3}>You claimed points for 1 ticket,</Text>
                            <Text style={styles.sheetfont4}>You have {fileTicket?.count - 1} left</Text>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                {/* dispatch(getTicketStatusUnclaimed({ ...fileTicket, ticketId: fileTicket._id, eventName: 'UNCLAIMED_POINTS' })) */}
                                <View style={styles.sheetBtn}
                                >
                                    <Button uppercase={false} labelStyle={{ fontSize: 19, fontWeight: '500' }}
                                        loading={loading} contentStyle={{
                                            backgroundColor: COLORS.primary, alignSelf: 'center'
                                        }}
                                        mode="contained"
                                        onPress={() => (setTimeout(() => {
                                            setdialogVisible(!dialogVisible)
                                            setloading(false)
                                        }, 2000), setloading(true))}
                                    >
                                        Save for Later
                                    </Button>
                                </View>
                                <View style={styles.sheetBtn}
                                >
                                    <Button uppercase={false} labelStyle={{ fontSize: 19, fontWeight: '500' }}
                                        loading={loading1} contentStyle={{
                                            backgroundColor: COLORS.primary, alignSelf: 'center'
                                        }}
                                        mode="contained" onPress={() => (setTimeout(() => {
                                            myCustomShare(fileTicket._id)
                                            setloading1(false)
                                        }, 2000), setloading1(true))}>
                                        Share Ticket
                                    </Button>
                                </View>
                            </View>
                        </>
                }
            </RBSheet>
        </View>
    )
};

export default Ticket;


