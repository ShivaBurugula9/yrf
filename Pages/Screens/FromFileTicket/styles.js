import { StyleSheet } from "react-native"
import { COLORS, FONTS, SIZES } from "../../Constants/theme"


export const styles = StyleSheet.create({
    body: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: SIZES.width,
        padding: 7,
        marginTop: 13
    },
    btn: {
        backgroundColor: COLORS.primary,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 24,
        height: 50,
        width: SIZES.width - 20,
        top: 10,
    },
    sheetBtn: {
        backgroundColor: COLORS.primary,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 11,
        height: 50,
        minWidth: 140, margin: 5
    },
    sheetBtn1: {
        backgroundColor: COLORS.primary,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 11,
        height: 50,
        width: 260,
    },
    square: {
        width: '25%',
        height: 140,
        borderRadius: 10,
        flex: 1,
    },
    font: {
        ...FONTS.font17,
        marginBottom: 12
    },
    font1: {
        ...FONTS.font15,
        marginTop: 15,
    },
    font2: {
        ...FONTS.font15,
        top: 6,
        fontSize: 13,
    },
    upperContainer: {
        backgroundColor: COLORS.primary,
        padding: 2,
        marginHorizontal: 7,
        alignItems: 'center',
        borderRadius: 14,
        width: '99%'
    },
    lowerHeading: {
        ...FONTS.font15,
        color: '#ADADB2',
        fontSize: 20,
        marginTop: 6
    },
    value: {
        ...FONTS.font15,
        fontSize: 20,
    },
    sheetfont1: {
        ...FONTS.font15,
        color: '#FFD233',
    },
    sheetfont2: {
        ...FONTS.font15,
        fontSize: 22,
        color: '#C4C4C4',
        alignSelf: 'center'
    },
    sheetfont3: {
        ...FONTS.font15,
        color: '#BA9DFF',
        fontSize: 23,
    },
    sheetfont4: {
        ...FONTS.font15,
        color: '#000000',
        fontSize: 23
    },
})