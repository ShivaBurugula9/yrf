import React, { useEffect, useRef, useState } from 'react';
import SwipeButton from 'rn-swipe-button';
import {
    View, Text, TouchableOpacity,
    Image, Animated, ScrollView,
} from 'react-native';
LogBox.ignoreLogs(["EventEmitter.removeListener"]);
import Clipboard from '@react-native-community/clipboard';
import Sheet from "../../Components/Sheet";
import AsyncStorageStatic from '@react-native-community/async-storage'
import dynamicLinks from '@react-native-firebase/dynamic-links';
import { useSelector, useDispatch } from 'react-redux'
import Vectr from '../../../assets/images/Vectr.png'
import { LogBox } from "react-native";
import Share from 'react-native-share';
import { UpdatePoints } from '../../redux/Actions/PointsAction';
import { closeDrawer, changeRefresh } from '../../redux/Actions/action'
import LinearGradiant from '../../Components/LinearGradiant';
import CustomSnack from '../../Components/CustomSnack';
import Coin from '../../Components/Coin';
import { styles } from './styles';
import { FONTS } from '../../Constants/theme';


function RewardDetails({ route, navigation }) {
    const { itemDetail } = route.params;
    const dispatch = useDispatch()
    const [dialogVisible, setdialogVisible] = useState(false)
    const [snackVisible, setSnackVisible] = useState(false)
    const translation = useRef(new Animated.Value(200)).current;
    const { show, refresh } = useSelector(state => state.Reducer)
    const { newPoints } = useSelector(state => state.user)
    const [dynamic, setdynamic] = useState('')

    if (show) {
        buildDynamicLink()
    }
    async function buildDynamicLink() {
        const userId = JSON.parse(await AsyncStorageStatic.getItem('UserId'))
        try {
            let shortLink = await dynamicLinks().buildLink({
                link: 'https://www.google.com?id=' + (userId),
                domainUriPrefix: 'https://gambt.page.link',
                android: {
                    packageName: 'com.yrf'
                }
            })
            return setdynamic(shortLink)
        } catch (error) {
            console.log(error)
        }
    }
    const myCustomShare = async () => {
        const shareOptions = {
            message: `${dynamic}`,
            // url : files.image
        }
        try {
            const ShareResponse = await Share.open(shareOptions)
            console.log(JSON.stringify(ShareResponse))
        } catch (error) {
            console.log('Error => ', error)
        }
    }
    useEffect(() => {
        if (refresh) {
            dispatch(UpdatePoints({ points: -itemDetail.points, rewardId: itemDetail._id, eventName: "POINTS_CLAIMED" }))
        }
    }, [refresh])
    useEffect(() => {
        Animated.timing(translation, {
            toValue: 0,
            useNativeDriver: false,
            duration: 500
        }).start()
        
        return () => dispatch(changeRefresh(false))
    }, [])
    return (
        <View style={styles.body}>
            <ScrollView>
                <Animated.View style={{
                    alignItems: 'center',
                    marginTop: translation
                }}>
                    <Image  style={styles.imageStyle} resizeMode='stretch'
                        source={{ uri: itemDetail.bannerImage }} />
                </Animated.View>
                <View style={{
                    justifyContent: 'space-between',
                    flexDirection: 'row', top: 10,
                     alignItems: 'center', padding: 8,
                }}>
                    <Text style={FONTS.font18}>{itemDetail.rewardName}</Text>
                    <Coin  points={itemDetail.points} />
                </View>
                {
                    refresh &&
                    <View style={styles.claimedContainer}>
                        <Text style={{ ...FONTS.font18, fontSize: 25, left: 10 }}>Claimed</Text>
                        <TouchableOpacity onPress={() => (Clipboard.setString(itemDetail.rewardCode), setSnackVisible(true))} style={styles.btnS}>
                            <Text style={{ color: '#BA9DFF', fontWeight: '500', fontSize: 20 }}>{itemDetail.rewardCode}</Text>
                            <Text style={{ color: '#fff' }}>Tap to copy</Text>
                        </TouchableOpacity>
                    </View>
                }
                <View style={styles.lowerWrapper}>
                    <Text style={FONTS.font18}>Reward Details</Text>
                    <Text style={{ ...FONTS.font15, color: '#787878' }}>{itemDetail.rewardDescription}</Text>
                    <Text style={FONTS.font18}>Terms & Conditions</Text>

                    {
                        itemDetail.termsAndCondition.map((item, i) => (
                            <View key={i}>
                                <Text style={{ ...FONTS.font15, color: '#787878' }}>{item.text}</Text>
                            </View>
                        ))
                    }
                </View>
                <Sheet show={show} onDismiss={() => {
                    dispatch(closeDrawer());
                }} onPress={() => (dispatch(closeDrawer(), myCustomShare()))}
                />
                <LinearGradiant text={'Rewards Claimed Successfully!'} dialogVisible={dialogVisible} setdialogVisible={setdialogVisible} />
            </ScrollView>
            <CustomSnack style={{ position: 'absolute', bottom: 5, zIndex: 20, }}
             success={true} snackVisible={snackVisible} setSnackVisible={setSnackVisible} snacktext={'Copied succesfully'} />
            <View style={{ position: 'absolute', bottom: 10, zIndex: 10 }}>
                {
                    !refresh ?
                        <SwipeButton
                            disabled={newPoints >= itemDetail.points ? false : true}
                            swipeSuccessThreshold={70}
                            height={55}
                            width={300}
                            title='Swipe right to claim'
                            titleColor='#777777'
                            titleFontSize={16}
                            thumbIconImageSource={Vectr}
                            onSwipeSuccess={() => {
                                setdialogVisible(true)
                                dispatch(changeRefresh(true))
                            }}
                            //After the completion of swipe (Optional)
                            railFillBackgroundColor="#38354B" //(Optional)
                            railFillBorderColor="#38354B" //(Optional)
                            thumbIconBackgroundColor="#ffff" //(Optional)
                            thumbIconBorderColor="#38354B" //(Optional)
                            railBackgroundColor="#38354B" //(Optional)
                            railBorderColor="#38354B" //(Optional)
                        />
                        :
                        <TouchableOpacity onPress={() => (navigation.navigate('ViewAll'))}>
                            <View style={styles.btn}
                            >
                                <Text style={{ ...FONTS.font18, fontSize: 20 }}>Collected More</Text>
                            </View>
                        </TouchableOpacity>
                }
            </View>
        </View>
    );
}

export default RewardDetails
