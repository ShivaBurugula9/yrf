import React, { useState } from 'react';
import { View,  StyleSheet,} from 'react-native';
import { useDispatch, useSelector } from 'react-redux'
import { UpdateUser } from '../../redux/Actions/userAction';
import { UpdatePoints } from '../../redux/Actions/PointsAction';
import CustomSnack from '../../Components/CustomSnack';
import CustomButton from '../../Components/CustomButton';
import DatePicker from 'react-native-date-picker'
import moment from 'moment';
export default function DatePickers() {
  const [newdate, setnewdate] = useState(new Date())
  const [snackVisible, setSnackVisible] = useState(false)
  const { userData } = useSelector(state => state.user)
  const dispatch = useDispatch()

  const submit = () => {
    if (userData.dateOfBirth === null) {
      dispatch(UpdatePoints({ points: 50, eventName: "DATE_POINTS" }))
    }
    return
  }
  const valueDate = moment(newdate).format('l')
  const actualdate = valueDate.split('/')
  const alldate = {
    month: actualdate[0],
    date: actualdate[1],
    year: actualdate[2]
  }
  const allValues = () => {
    (dispatch(UpdateUser({ alldate, value: 'dateOfBirth' })),
      submit(), setSnackVisible(true))
    return
  }
  return (
    <View style={styles.body}>
      <DatePicker mode='date' date={new Date(userData.dateOfBirth)} onDateChange={setnewdate} />
      <CustomButton btntext={userData?.dateOfBirth ? "Update" : 'Add'} onPress={allValues} />
      <CustomSnack success={true} snackVisible={snackVisible} setSnackVisible={setSnackVisible} snacktext={'Date Updated Successfully'} />
    </View>
  );
}
const styles = StyleSheet.create({
  body: {
    alignItems: 'center',
    left: 15,
    flex: 1
  },
})

