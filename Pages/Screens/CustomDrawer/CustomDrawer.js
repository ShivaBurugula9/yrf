import React, {  useState } from 'react';
import { View, Text, Image,  TouchableOpacity, ScrollView, StyleSheet, TextInput, Pressable } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
    DrawerContentScrollView, DrawerItemList,
    DrawerItem
} from '@react-navigation/drawer';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import DatePickerApp from './DatePicker';
import AddressField from './AddressField'
import Gender from './Gender';
import { Dialog } from 'react-native-simple-dialogs';
import { logout } from '../../redux/Actions/userAction';
import { COLORS, FONTS, SIZES } from '../../Constants/theme';


export default function CustomDrawer(props) {
    const { userData } = useSelector(state => state.user)
    const dispatch = useDispatch()
    const [state, setState] = useState(false)
    const [dialogVisible, setDialogVisible] = useState(false)
    const [dateField, setDateField] = useState(false)
    const [addressField, setAddressField] = useState(false)
    const [genderField, setgenderField] = useState(false)
    const Points = () => (
        <View style={{ borderRadius: 8, backgroundColor: COLORS.darkpurple, maxWidth: 72, alignItems: 'center', left: 31, padding: 2 }}>
            <Text style={{ color: '#fff', fontSize: 11 }}>Earn 50 Pts</Text>
        </View>
    )
    const openDate = () => (
        setDateField(!dateField),
        setAddressField(false),
        setgenderField(false)
    )
    const openAddress = () => (
        setAddressField(!addressField),
        setDateField(false),
        setgenderField(false)
    )
    const openGender = () => (
        setgenderField(!genderField),
        setDateField(false),
        setAddressField(false)
    )
    const toggleDrawer = () => (
        props.navigation.toggleDrawer(),
        setState(false),
        setDateField(false),
        setAddressField(false),
        setgenderField(false)
    )
    const ClaimedImage = () => (
        <View style={styles.claimedWrapper}>
            <Image source={require('../../../assets/images/Tick.png')} />
            <Text style={styles.claimed}>Claimed</Text>
        </View>
    )
    return (
        <DrawerContentScrollView  {...props}
            contentContainerStyle={{ backgroundColor: COLORS.primary, width: '100%', height: '100%' }} >
            <TouchableOpacity onPress={() => toggleDrawer()}>
                <Ionicons name='close' style={{ color: 'white', fontSize: 32, marginLeft: 'auto', padding: 10 }} />
            </TouchableOpacity>
            <View style={{ alignItems: 'center', top: -18 }}>
                <View style={{
                    alignItems: 'center',
                    justifyContent: 'center', height: 150, width: 150,
                    borderRadius: 75, backgroundColor: COLORS.gray
                }}>
                    <Text style={{ ...FONTS.font18, fontSize: 88 }}>{userData?.firstName[0]}</Text>
                </View>
                <Text style={{...FONTS.font18, margin: 5 }}>{userData?.firstName}</Text>
                <Text style={{ ...FONTS.font15, margin: 5 }}>{userData?.phoneNo}</Text>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center', width: '100%',
                    justifyContent: 'center'
                }}>
                    <Text style={{...FONTS.font18, marginRight: 16, marginLeft: 25
                    }}>Profile Completeness</Text>
                    <AnimatedCircularProgress
                        size={70}
                        width={10}
                        fill={(userData?.dateOfBirth && 33.3 || 0) + (userData?.address && 33.3 || 0) + (userData?.gender && 33.3 || 0)}
                        tintColor={COLORS.darkpurple}
                        backgroundColor={COLORS.gray} >
                        {fill => <Text style={{
                            color: '#fff',
                            fontWeight: 'bold', fontSize: 15,
                        }} >{`${Math.ceil(fill)}%`}</Text>}
                    </AnimatedCircularProgress>
                </View>
            </View>
            <Dialog
                visible={dialogVisible}
                dialogStyle={
                    {
                        backgroundColor: '#BA9DFF',
                        borderRadius: 12, height: SIZES.height - 80, padding: 10
                    }
                }
                onTouchOutside={() => setDialogVisible(false)} >
                <View style={{ overflow: 'scroll' }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={styles.heading}>Terms and Conditions</Text>
                        <TouchableOpacity onPress={() => setDialogVisible(false)}>
                            <Ionicons name='close' style={{ color: '#ffff', fontSize: 35, fontWeight: 'bold' }} />
                        </TouchableOpacity>
                    </View>
                    <Text style={FONTS.font17}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Condimentum dictum accumsan sed rutrum purus lorem tempus. Mattis tempor, vel neque, at lectus senectus non. Morbi ante iaculis volutpat, dictum sed elementum. Gravida pharetra pellentesque ornare turpis dignissim pharetra. Sed ultricies molestie amet, turpis eu nec etiam diam viverra.
                    </Text>
                    <Text style={styles.heading}>Help and support</Text>
                    <Text style={FONTS.font17}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Condimentum dictum accumsan sed rutrum purus lorem tempus. Mattis tempor, vel neque, at lectus senectus non. Morbi ante iaculis volutpat, dictum sed elementum. Gravida pharetra pellentesque ornare turpis dignissim pharetra. Sed ultricies molestie amet, turpis eu nec etiam diam viverra.
                    </Text>
                </View>
            </Dialog>
            <ScrollView>
                <DrawerItem
                    label={(focus, color) => (
                        <View style={{ flexDirection: 'row', alignItems: 'center', left: 4, }}>
                            {
                                !state && <>
                                    <FontAwesome5 name='user' style={{ right: 2 }} size={20} color='#fff' />
                                    <View style={{ flexDirection: 'row', alignItems: 'center', left: 2 }}>
                                        <Text style={styles.fonts}>User Profile</Text>
                                        <View style={{
                                            borderRadius: 8, backgroundColor: COLORS.darkpurple,
                                            width: 50, alignItems: 'center', left: 80, padding: 1
                                        }}>
                                            <Text style={{ ...FONTS.font17, fontSize: 11 }}>Earn </Text>
                                        </View>
                                    </View>
                                </>
                            }
                        </View>
                    )}
                    onPress={() => setState(true)}
                />
                {
                    state ?
                        <View style={{ width: '97%', top: -40 }}>
                            <View>
                                <View style={styles.eachItem}>
                                    <TouchableOpacity onPress={() => openDate()}>
                                        <Text style={styles.fonts2}>DOB (Date of Birth)</Text>
                                    </TouchableOpacity>
                                    {
                                        userData.dateOfBirth && <ClaimedImage />
                                    }
                                </View>
                                {
                                    dateField ?
                                        <DatePickerApp />
                                        :
                                        !userData.dateOfBirth &&
                                        <Points />
                                }
                                <View>
                                    <View style={styles.eachItem}>
                                        <TouchableOpacity onPress={() => openAddress()}>
                                            <Text style={styles.fonts2}>Address</Text>
                                        </TouchableOpacity>
                                        {
                                            userData.address && <ClaimedImage />
                                        }
                                    </View>
                                    {
                                        addressField ? <AddressField /> :
                                            !userData.address &&
                                            <Points />
                                    }
                                </View>
                                <View>
                                    <View style={styles.eachItem}>
                                        <Pressable onPress={() => openGender()} >
                                            <Text style={styles.fonts2}>Gender</Text>
                                        </Pressable>
                                        {
                                            userData.gender && <ClaimedImage />

                                        }
                                    </View>
                                    {
                                        genderField ? <Gender /> :
                                            !userData.gender &&
                                            <Points />

                                    }
                                </View>
                            </View>
                        </View>
                        : (<ScrollView>
                            <DrawerItemList {...props} />
                            <DrawerItem
                                style={{ top: -29, left: 3 }}
                                label={(focus, color) => (
                                    <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                                        {
                                            !state && <>
                                                <FontAwesome5 name='history' size={20} color='#fff' />
                                                <Text style={styles.fonts}>Reward History</Text>
                                            </>
                                        }
                                    </View>
                                )}
                                onPress={() => (props.navigation.navigate('Reward History'), props.navigation.toggleDrawer())}
                            />
                            <DrawerItem
                                style={{ top: -25, }}
                                label={(focus, color) => (
                                    <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                                        {
                                            !state && <>
                                                <MaterialIcons name='support-agent' size={24} color='#fff' />
                                                <Text style={styles.fonts}>Help Support</Text>
                                            </>
                                        }
                                    </View>
                                )}
                                onPress={() => setDialogVisible(true)}
                            />
                            <TouchableOpacity style={{ alignSelf: 'center' }} onPress={() => dispatch(logout())}>
                                <View style={styles.btn}
                                >
                                    <Text style={FONTS.font18}>Logout</Text>
                                </View>
                            </TouchableOpacity>
                        </ScrollView>)
                }
            </ScrollView>
        </DrawerContentScrollView>
    );
}

const styles = StyleSheet.create({
    btn: {
        backgroundColor: '#1C1A29',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
        height: 45,
        marginTop: 10,
        width: 200,
    },
    heading: {
        fontSize: 22,
        color: '#fff',
        fontWeight: '500',
        marginBottom: 14,
        top: 5,
        right: 10
    },
    fonts: {
        ...FONTS.font18,
        marginLeft: 29,
        fontSize: 17
    },
    fonts2: {
        ...FONTS.font15,
        marginLeft: 29,
        margin: 11,
    },
    eachItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '100%',
        marginTop: 10
    },
    claimed: {
        fontSize: 14,
        color: '#ffff',
        fontWeight: '500',
        marginLeft: 10
    },
    claimedWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10
    }
})
