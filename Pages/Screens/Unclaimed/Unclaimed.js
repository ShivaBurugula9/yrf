import React, { useEffect, useState } from 'react'
import { View, FlatList, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image } from 'react-native';
import { useSelector, useDispatch } from 'react-redux'
import dynamicLinks from '@react-native-firebase/dynamic-links';
import Share from 'react-native-share';
import moment from "moment";
import AsyncStorageStatic from '@react-native-community/async-storage'
import CustomLoader from '../../Components/CustomLoader';
import { FONTS, SIZES } from '../../Constants/theme';


const Unclaimed = () => {
    const [isLoading, setisLoading] = useState(true)
    const { history } = useSelector(state => state.user)
    const myCustomShare = async (ticketId) => {
        const userId = JSON.parse(await AsyncStorageStatic.getItem('UserId'))
        try {
            let shortLink = await dynamicLinks().buildLink({
                link: `https://www.google.com?ticketId=${ticketId}&userId=${userId}`,
                domainUriPrefix: 'https://gambt.page.link',
                android: {
                    packageName: 'com.yrf'
                }
            })
            const shareOptions = {
                message: `${shortLink}`,
                // url : files.image
            }
            const ShareResponse = await Share.open(shareOptions)
            console.log(JSON.stringify(ShareResponse))
        } catch (error) {
            console.log('Error => ', error)
        }
    }
    useEffect(() => {
        setTimeout(() => {
            setisLoading(false)
        }, 2000);
    }, [isLoading])
    if (isLoading) {
        return <CustomLoader />
    }

    let mapData = history.filter((item) => item.eventName === 'UNCLAIMED_POINTS' && item.ticketId.status !== 'Rejected' && item.ticketId.status == 'Approved')
    const renderItem = ({ item }) => (
        <View style={styles.scroll}>
            <View style={{ width: '32%', height: 150 }}>
                <ImageBackground resizeMode='stretch' style={styles.image} source={{ uri: item?.rewardId?.bannerImage || item?.ticketId?.ticketImage }} >
                    <View style={{ width: 100, height: 150 }}>
                    </View>
                </ImageBackground>
                {/* <Image resizeMode='stretch' style={styles.image} source={{ uri: item?.ticketId?.ticketImage || item?.ticketImage }} /> */}
            </View>
            <View style={{
                left: 7,
                width: '66%', height: '100%',
                flexDirection: 'row', padding: 2
            }}>
                <View style={{ width: '80%' }}>
                    <Text style={styles.moviewheader}>{item?.ticketId?.movieName || item?.movieName || 'others'}</Text>
                    <Text style={styles.font}>Points earned via {item?.ticketId?.source}</Text>
                    <Text style={styles.font}>{moment(item?.ticketId?.createdAt).format('llll')}</Text>
                    <Text style={{ color: "#F99601", fontSize: 14, marginTop: 10 }}>{item?.ticketId?.status}</Text>
                </View>
                <View style={{ justifyContent: 'space-between', width: '20%' }}>
                    <View style={{
                        alignItems: 'center', justifyContent: 'center',
                        backgroundColor: '#F99601',
                        borderRadius: 12, width: 44, height: 20, top: 8
                    }}>
                        <Text style={{ color: '#fff', fontSize: 11, fontWeight: '500' }}>10 Pts</Text>
                    </View>
                    <TouchableOpacity onPress={() => myCustomShare(item?.ticketId?._id)} style={{
                        alignItems: 'center', justifyContent: 'center',
                        backgroundColor: '#BA9DFF',
                        borderRadius: 12, width: 44, height: 20, bottom: 12
                    }}>
                        <Text style={{ color: '#fff', fontSize: 11, fontWeight: '500' }}>Share</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
    return (
        <View style={styles.body}>
            <Text style={styles.header}>Unclaimed Pts</Text>
            {
                history.length ?
                    <FlatList
                        data={mapData}
                        renderItem={renderItem}
                        keyExtractor={item => item._id}
                    /> : <View style={{ alignSelf: 'center', justifyContent: 'center' }}>
                        <Text style={{ color: 'black', fontSize: 41, fontWeight: '500' }}>Empty </Text>
                    </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        padding: 8,
        width: SIZES.width
    },
    image: {
        width: '100%',
        borderRadius: 12,
        overflow: 'hidden',
    },
    header: {
        color: '#BA9DFF',
        fontSize: 24,
        fontWeight: '500',
        padding: 20
    },
    font: {
        ...FONTS.font17,
        fontSize: 15,
        color: 'black',
        top: 10,
        bottom: 10
    },
    moviewheader: {
        ...FONTS.font17,
        marginBottom: 8,
        top: 5,
        color: 'black'
    },
    scroll: {
        flexDirection: 'row',
        padding: 7,
        alignItems: 'center',
        bottom: 15,
        top: 10
    }
})
export default Unclaimed

