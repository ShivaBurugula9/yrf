import {StyleSheet} from 'react-native'
import {COLORS,FONTS,SIZES} from '../../Constants/theme'

export const styles = StyleSheet.create({
    body: {
        flex: 1,
        backgroundColor: COLORS.background,
        alignItems: 'center',
        padding: 18,
        paddingBottom: 58,
        height: SIZES.height
    },
    topWrapper: {
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 2
    },
    lowerWrapper: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        padding: 10,
    },
    fonts: {
        ...FONTS.font18,
        fontSize: 28,
        alignSelf: 'center',
        margin: 5
    },
    btn: {
        alignSelf: 'center',
        height: 55,
        width: SIZES.width - 60,
    },
    searchSection: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: COLORS.secondary,
        width: SIZES.width - 50,
        borderRadius: 8,
        margin: 6,
        padding: 6
    },
    searchIcon: {
        padding: 10,
        fontSize: 20
    },
    input: {
        paddingTop: 8,
        paddingRight: 8,
        fontSize: 20,
        paddingLeft: 8,
        paddingBottom: 8,
        color: '#fff',
        width: '90%'
    },
})