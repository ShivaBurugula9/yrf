import React, { useState } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, TextInput,
     ScrollView,  } from 'react-native';
import * as yup from 'yup';
import { Formik } from 'formik'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { useDispatch } from 'react-redux';
import { UserRegister } from '../../redux/Actions/userAction';
import { Button } from 'react-native-paper';
import { COLORS, FONTS, SIZES } from '../../Constants/theme';
import { styles } from './styles';
export default function Register({ navigation }) {
    const [showPassword, setshowPassword] = useState(true)
    const [loading, setloading] = useState(false)
    const [refferalcode, setRefferalcode] = useState('')
    const dispatch = useDispatch()
    const ValidationSchema = yup.object().shape({
        firstName: yup.string().required('First name must required').
            matches(/^[aA-zZ\s]+$/, "Only alphabets are allowed for this field "),
        phoneNo: yup.string().
            min(10, 'Phone number must be 10 digits').
            max(10, 'Phone number must be 10 digits').
            required(' Phone number must required'),
        email: yup.string().email('Please enter valid email').required('Email must required'),
        password: yup.string().min(4, ({ min }) => `Password must be atleast ${min} digits`).
            required(' Password must required'),
    });
    return (
        <>
            <Formik
                initialValues={{ email: '', firstName: '', phoneNo: '', password: '' }}
                // initialValues={{ email: 'jit@gmail.com', firstName: 'Nilu', phoneNo: '0000000002', password: 'Test' }}
                onSubmit={values => dispatch(UserRegister(values, navigation.navigate))}
                validateOnMount={true}
                validationSchema={ValidationSchema}
            >
                {({ handleChange, handleBlur, handleSubmit, touched, errors, values }) => (
                    <ScrollView>
                        <View style={styles.body}>
                            <View style={styles.topWrapper}>
                                <Text style={styles.fonts}>Welcome</Text>
                                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', width: SIZES.width, marginTop: 20 }}>
                                    <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                                        <Text style={{ ...FONTS.font18, marginLeft: 20 }}>Login </Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity>
                                        <Text style={{...FONTS.font18, bottom: 1 }}>/Sign up </Text>
                                    </TouchableOpacity>
                                </View>
                                <View>
                                    <View style={styles.searchSection}>
                                        <TextInput
                                            style={styles.input}
                                            placeholder='Enter Name'
                                            placeholderTextColor='#5A5A5A'
                                            onChangeText={handleChange('firstName')}
                                            value={values.firstName}
                                            underlineColorAndroid="transparent"
                                        />
                                    </View>
                                    {(errors.firstName && touched.firstName) &&
                                        <View style={{ marginRight: 'auto', left: 10 }}>
                                            <Text style={FONTS.h5}>{errors.firstName}</Text>
                                        </View>}
                                    <View style={styles.searchSection}>
                                        <TextInput
                                            style={styles.input}
                                            placeholder='Phone Number'
                                            keyboardType='numeric'
                                            onBlur={handleBlur('phoneNo')}
                                            placeholderTextColor='#5A5A5A'
                                            onChangeText={handleChange('phoneNo')}
                                            value={values.phoneNo}
                                            underlineColorAndroid="transparent"
                                        />
                                    </View>
                                    {(errors.phoneNo && touched.phoneNo) &&
                                        <View style={{ marginRight: 'auto', left: 10 }}>
                                            <Text style={FONTS.h5}>{errors.phoneNo}</Text>
                                        </View>}
                                    <View style={styles.searchSection}>
                                        <TextInput
                                            style={styles.input}
                                            placeholder='Email'
                                            keyboardType='email-address'
                                            placeholderTextColor='#5A5A5A'
                                            onChangeText={handleChange('email')}
                                            onBlur={handleBlur('email')}
                                            value={values.email}
                                            underlineColorAndroid="transparent"
                                        />
                                    </View>
                                    {(errors.email && touched.email) &&
                                        <View style={{ marginRight: 'auto', left: 10 }}>
                                            <Text style={FONTS.h5}>{errors.email}</Text>
                                        </View>}
                                    <View style={styles.searchSection}>
                                        <TextInput
                                            style={styles.input}
                                            placeholder='Password'
                                            placeholderTextColor='#5A5A5A'
                                            onChangeText={handleChange('password')}
                                            onBlur={handleBlur('password')}
                                            value={values.password}
                                            secureTextEntry={showPassword}
                                            underlineColorAndroid="transparent"
                                        />
                                        <FontAwesome5 onPress={() => setshowPassword(!showPassword)} style={styles.searchIcon} name={!showPassword ? "eye" : "eye-slash"} size={40} color='#5A5A5A' />
                                    </View>
                                    {(errors.password && touched.password) &&
                                        <View style={{ marginRight: 'auto', left: 10 }}>
                                            <Text style={FONTS.h5}>{errors.password}</Text>
                                        </View>}
                                </View>
                            </View>
                            <View style={styles.lowerWrapper}>
                                <Text style={{ fontSize: 18, color: '#fff', marginLeft: 70, width: SIZES.width, fontWeight: '700' }}>If Have Any(Optional) </Text>
                                <View style={styles.searchSection}>
                                    <TextInput
                                        style={styles.input}
                                        placeholder='Enter Referal Code'
                                        placeholderTextColor='#5A5A5A'
                                        onChangeText={(value) => setRefferalcode(value)}
                                        underlineColorAndroid="transparent"
                                    />
                                </View>
                                <Button uppercase={false}
                                    labelStyle={{ fontSize: 19,
                                     fontWeight: '500' }}
                                    loading={loading}
                                    color={COLORS.primary}
                                    style={{top:20,borderRadius:12}}
                                    contentStyle={styles.btn}
                                    mode="contained" onPress={() => (setTimeout(() => {
                                        handleSubmit()
                                        setloading(false)
                                    }, 1000), setloading(true))}>
                                    Register
                                </Button>
                            </View>
                        </View>
                    </ScrollView>
                )}
            </Formik>
        </>
    );
}


