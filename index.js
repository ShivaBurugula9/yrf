/* eslint-disable prettier/prettier */
/**
 * @format
 */
import * as React from 'react';
import { AppRegistry } from 'react-native';
import App from './App';
import { Provider } from 'react-redux'
import { Store } from './Pages/redux/store';
import { name as appName } from './app.json';
import { Provider as PaperProvider } from 'react-native-paper'

export default function Main() {
  return (
    <PaperProvider>
      <Provider store={Store}>
        <App />
      </Provider>
    </PaperProvider>
  );
}


AppRegistry.registerComponent(appName, () => Main);
